#!/usr/bin/env ruby
#
# 

require 'pp'

class DrumTester
    DRUMSET = [
	       {
		   :name => 'STANDARD 1',
		   :prog => 1,
		   :list => [
			     '[88] Standard1 Kick1', #0
			     '[88] Standard1 Kick2', #1
			     '[88] Standard2 Kick1', #2
			     '[88] Standard2 Kick2', #3
			     '[55] Kick Drum1', #4
			     '[55] Kick Drum2', #5
			     '[88] Jazz Kick1', #6
			     '[88] Jazz Kick2', #7
			     '[88] Room Kick1', #8
			     '[88] Room Kick2', #9
			     '[88] Power Kick1', #10
			     '[88] Power Kick2', #11
			     '[88] Electric Kick2', #12
			     '[88] Electric Kick1', #13
			     '[88] TR-808 Kick', #14
			     '[88] TR-909 Kick', #15
			     '[88] Dance Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     'MC-505 Beep1', #22
			     'MC-505 Beep2', #23
			     'Concert SD', #24
			     'Snare Roll', #25
			     'Finger Snap2', #26
			     'High Q', #27
			     'Slap', #28
			     'Scratch Push', #29
			     'Scratch Pull', #30
			     'Sticks', #31
			     'Square Click', #32
			     'Metronome Click', #33
			     'Metronome Bell', #34
			     'Kick Drum 2', #35
			     'Kick Drum 1', #36
			     'Side Stick', #37
			     'Snare Drum 1', #38
			     'Hand Clap', #39
			     'Snare Drum 2', #40
			     'Low Tom 2', #41
			     'Closed Hi-Hat', #42
			     'Low Tom 1', #43
			     'Pedal Hi-Hat', #44
			     'Mid Tom 2', #45
			     'Open Hi-Hat', #46
			     'Mid Tom 1', #47
			     'High Tom 2', #48
			     'Crash Cymbal 1', #49
			     'High Tom 1', #50
			     'Ride Cymbal 1', #51
			     'Chinese Cymbal', #52
			     'Ride Bell', #53
			     'Tambourine', #54
			     'Splash Cymbal', #55
			     'Cowbell', #56
			     'Crash Cymbal 2', #57
			     'Vibra-slap', #58
			     'Ride Cymbal 2', #59
			     'High Bongo', #60
			     'Low Bongo', #61
			     'Mute High Conga', #62
			     'Open High Conga', #63
			     'Low Conga', #64
			     'High Timbale', #65
			     'Low Timbale', #66
			     'High Agogo', #67
			     'Low Agogo', #68
			     'Cabasa', #69
			     'Maracas', #70
			     'Short Hi Whistle', #71
			     'Long Low Whistle', #72
			     'Short Guiro', #73
			     'Long Guiro', #74
			     'Claves', #75
			     'High Wood Block', #76
			     'Low Wood Block', #77
			     'Mute Cuica', #78
			     'Open Cuica', #79
			     'Mute Triangle', #80
			     'Open Triangle', #81
			     'Shaker', #82
			     'Jingle Bell', #83
			     'Belltree', #84
			     'Castanets', #85
			     'Mute Surdo', #86
			     'Open Surdo', #87
			     'Applaus2', #88
			     nil, #89
			     nil, #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     '[88] Standard1 Snare1', #97
			     '[88] Standard1 Snare2', #98
			     '[88] Standard2 Snare1', #99
			     '[88] Standard2 Snare2', #100
			     '[55] Snare Drum2', #101
			     'Standard1 Snare1', #102
			     'Standard1 Snare2', #103
			     'Standard Snare3', #104
			     '[88] Jazz Snare1', #105
			     '[88] Jazz Snare2', #106
			     '[88] Room Snare1', #107
			     '[88] Room Snare2', #108
			     '[88] Power Snare1', #109
			     '[88] Power Snare2', #110
			     '[55] Gated Snare', #111
			     '[88] Dance Snare1', #112
			     '[88] Dance Snare2', #113
			     '[88] Disco Snare', #114
			     '[88] Electric Snare2', #115
			     '[55] Electric Snare', #116
			     '[88] Electric Snare 3', #117
			     'TR-707 Snare', #118
			     '[88] TR-808 Snare1', #119
			     '[88] TR-808 Snare2', #120
			     '[88] TR-909 Snare1', #121
			     '[88] TR-909 Snare2', #122
			     'Rap Snare', #123
			     'Jungle Snare1', #124
			     'House Snare1', #125
			     '[88] House Snare', #126
			     'House Snare2', #127
			   ],
	       },
	       {
		   :name => 'STANDARD 2',
		   :prog => 2,
 		   :list => [
			     '[88] Standard1 Kick1', #0
			     '[88] Standard1 Kick2', #1
			     '[88] Standard2 Kick1', #2
			     '[88] Standard2 Kick2', #3
			     '[55] Kick Drum1', #4
			     '[55] Kick Drum2', #5
			     '[88] Jazz Kick1', #6
			     '[88] Jazz Kick2', #7
			     '[88] Room Kick1', #8
			     '[88] Room Kick2', #9
			     '[88] Power Kick1', #10
			     '[88] Power Kick2', #11
			     '[88] Electric Kick2', #12
			     '[88] Electric Kick1', #13
			     '[88] TR-808 Kick', #14
			     '[88] TR-909 Kick', #15
			     '[88] Dance Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     'MC-505 Beep1', #22
			     'MC-505 Beep2', #23
			     'Concert SD', #24
			     'Snare Roll', #25
			     'Finger Snap', #26
			     'High Q', #27
			     'Slap', #28
			     'Scratch Push', #29
			     'Scratch Pull', #30
			     'Sticks', #31
			     'Square Click', #32
			     'Metronome Click', #33
			     'Metronome Bell', #34
			     'Standard2 Kick2', #35
			     'Standard2 Kick1', #36
			     'Side Stick', #37
			     'Standard2 Snare1', #38
			     'TR-808 Hand Clap', #39
			     'Standard2 Snare2', #40
			     'Low Tom2', #41
			     'Closed Hi-Hat2', #42
			     'Low Tom1', #43
			     'Pedal Hi-Hat2', #44
			     'Mid Tom2', #45
			     'Open Hi-Hat2', #46
			     'Mid Tom1', #47
			     'High Tom2', #48
			     'Crash Cymbal1', #49
			     'High Tom1', #50
			     'Ride Cymbal1', #51
			     'Chinese Cymbal', #52
			     'Ride Bell', #53
			     'Tambourine', #54
			     'Splash Cymbal', #55
			     'Cowbell', #56
			     'Crash Cymbal2', #57
			     'Vibra-slap', #58
			     'Ride Cymbal2', #59
			     'High Bongo', #60
			     'Low Bongo', #61
			     'Mute High Conga', #62
			     'Open High Conga', #63
			     'Open Low Conga', #64
			     'High Timbale', #65
			     'Low Timbale', #66
			     'High Agogo', #67
			     'Low Agogo', #68
			     'Cabasa', #69
			     'Maracas', #70
			     'Short High Whisle', #71
			     'Long Low Whisle', #72
			     'Short Guiro', #73
			     'Long Guiro', #74
			     'Claves', #75
			     'High Wood Block', #76
			     'Low Wood Block', #77
			     'Mute Cuica', #78
			     'Open Cuica', #79
			     'Mute Triangle', #80
			     'Open Triangle', #81
			     'Shaker', #82
			     'Jingle Bell', #83
			     'Bar Chimes', #84
			     'Castanets', #85
			     'Mute Surdo', #86
			     'Open Surdo', #87
			     'Applaus2', #88
			     nil, #89
			     nil, #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     '[88] Standard1 Snare1', #97
			     '[88] Standard1 Snare2', #98
			     '[88] Standard2 Snare1', #99
			     '[88] Standard2 Snare2', #100
			     '[55] Snare Drum2', #101
			     'Standard1 Snare1', #102
			     'Standard1 Snare2', #103
			     'Standard Snare3', #104
			     '[88] Jazz Snare1', #105
			     '[88] Jazz Snare2', #106
			     '[88] Room Snare1', #107
			     '[88] Room Snare2', #108
			     '[88] Power Snare1', #109
			     '[88] Power Snare2', #110
			     '[55] Gated Snare', #111
			     '[88] Dance Snare1', #112
			     '[88] Dance Snare2', #113
			     '[88] Disco Snare', #114
			     '[88] Electric Snare2', #115
			     '[55] Electric Snare', #116
			     '[88] Electric Snare 3', #117
			     'TR-707 Snare', #118
			     '[88] TR-808 Snare1', #119
			     '[88] TR-808 Snare2', #120
			     '[88] TR-909 Snare1', #121
			     '[88] TR-909 Snare2', #122
			     'Rap Snare', #123
			     'Jungle Snare1', #124
			     'House Snare1', #125
			     '[88] House Snare', #126
			     'House Snare2', #127
			   ],
	       },
	       {
		   :name => 'STANDARD 3',
		   :prog => 3,
		   :list => [
			     '[88] Standard1 Kick1', #0
			     '[88] Standard1 Kick2', #1
			     '[88] Standard2 Kick1', #2
			     '[88] Standard2 Kick2', #3
			     '[55] Kick Drum1', #4
			     '[55] Kick Drum2', #5
			     '[88] Jazz Kick1', #6
			     '[88] Jazz Kick2', #7
			     '[88] Room Kick1', #8
			     '[88] Room Kick2', #9
			     '[88] Power Kick1', #10
			     '[88] Power Kick2', #11
			     '[88] Electric Kick2', #12
			     '[88] Electric Kick1', #13
			     '[88] TR-808 Kick', #14
			     '[88] TR-909 Kick', #15
			     '[88] Dance Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     'MC-505 Beep1', #22
			     'MC-505 Beep2', #23
			     'Concert SD', #24
			     'Snare Roll', #25
			     'Finger Snap2', #26
			     'High Q', #27
			     'Slap', #28
			     'Scratch Push', #29
			     'Scratch Pull', #30
			     'Sticks', #31
			     'Square Click', #32
			     'Metronome Click', #33
			     'Metronome Bell', #34
			     'Standard3 Kick2', #35
			     '[RND] Kick', #36
			     'Side Stick', #37
			     '[RND] Snare', #38
			     '[RND] Hand Clap', #39
			     'Standard3 Snare2', #40
			     'Low Tom2', #41
			     '[RND] Closed Hi-Hat', #42
			     'Low Tom1', #43
			     '[RND] Pedal Hi-Hat', #44
			     'Mid Tom2', #45
			     '[RND] Open Hi-Hat', #46
			     'Mid Tom1', #47
			     'High Tom2', #48
			     '[RND] Crash Cymbal', #49
			     'High Tom1', #50
			     '[RND] Ride Cymbal1', #51
			     'Chinese Cymbal', #52
			     '[RND] Ride Bell', #53
			     'Tambourine', #54
			     'Splash Cymbal', #55
			     'Cowbell', #56
			     'Crash Cymbal2', #57
			     'Vibra-slap', #58
			     '[RND] Ride Cymbal2', #59
			     'High Bongo', #60
			     'Low Bongo', #61
			     'Mute High Conga', #62
			     'Open High Conga', #63
			     'Open Low Conga', #64
			     'High Timbale', #65
			     'Low Timbale', #66
			     'High Agogo', #67
			     'Low Agogo', #68
			     'Cabasa', #69
			     'Maracas', #70
			     'Short High Whisle', #71
			     'Long Low Whisle', #72
			     'Short Guiro', #73
			     'Long Guiro', #74
			     'Claves', #75
			     'High Wood Block', #76
			     'Low Wood Block', #77
			     'Mute Cuica', #78
			     'Open Cuica', #79
			     'Mute Triangle', #80
			     'Open Triangle', #81
			     'Shaker', #82
			     'Jingle Bell', #83
			     'Bell Tree', #84
			     'Castanets', #85
			     'Mute Surdo', #86
			     'Open Surdo', #87
			     'Applaus2', #88
			     nil, #89
			     nil, #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     '[88] Standard1 Snare1', #97
			     '[88] Standard1 Snare2', #98
			     '[88] Standard2 Snare1', #99
			     '[88] Standard2 Snare2', #100
			     '[55] Snare Drum2', #101
			     'Standard1 Snare1', #102
			     'Standard1 Snare2', #103
			     'Standard Snare3', #104
			     '[88] Jazz Snare1', #105
			     '[88] Jazz Snare2', #106
			     '[88] Room Snare1', #107
			     '[88] Room Snare2', #108
			     '[88] Power Snare1', #109
			     '[88] Power Snare2', #110
			     '[55] Gated Snare', #111
			     '[88] Dance Snare1', #112
			     '[88] Dance Snare2', #113
			     '[88] Disco Snare', #114
			     '[88] Electric Snare2', #115
			     '[55] Electric Snare', #116
			     '[88] Electric Snare 3', #117
			     'TR-707 Snare', #118
			     '[88] TR-808 Snare1', #119
			     '[88] TR-808 Snare2', #120
			     '[88] TR-909 Snare1', #121
			     '[88] TR-909 Snare2', #122
			     'Rap Snare', #123
			     'Jungle Snare1', #124
			     'House Snare1', #125
			     '[88] House Snare', #126
			     'House Snare2', #127
			   ],
	       },
	       {
		   :name => 'ROOM',
		   :prog => 9,
		   :list => [
			     '[88] Standard1 Kick1', #0
			     '[88] Standard1 Kick2', #1
			     '[88] Standard2 Kick1', #2
			     '[88] Standard2 Kick2', #3
			     '[55] Kick Drum1', #4
			     '[55] Kick Drum2', #5
			     '[88] Jazz Kick1', #6
			     '[88] Jazz Kick2', #7
			     '[88] Room Kick1', #8
			     '[88] Room Kick2', #9
			     '[88] Power Kick1', #10
			     '[88] Power Kick2', #11
			     '[88] Electric Kick2', #12
			     '[88] Electric Kick1', #13
			     '[88] TR-808 Kick', #14
			     '[88] TR-909 Kick', #15
			     '[88] Dance Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     'MC-505 Beep1', #22
			     'MC-505 Beep2', #23
			     'Concert SD', #24
			     'Snare Roll', #25
			     'Finger Snap', #26
			     'High Q', #27
			     'Slap', #28
			     'Scratch Push', #29
			     'Scratch Pull', #30
			     'Sticks', #31
			     'Square Click', #32
			     'Metronome Click', #33
			     'Metronome Bell', #34
			     'Kick Drum 2', #35
			     'Kick Drum 1', #36
			     'Side Stick', #37
			     'Snare Drum 1', #38
			     'Hand Clap', #39
			     'Snare Drum 2', #40
			     'Room Low Tom 2', #41
			     'Closed Hi-Hat', #42
			     'Room Low Tom 1', #43
			     'Pedal Hi-Hat', #44
			     'Room Mid Tom 2', #45
			     'Open Hi-Hat', #46
			     'Room Mid Tom 1', #47
			     'Room Hi Tom 2', #48
			     'Crash Cymbal 1', #49
			     'Room Hi Tom 1', #50
			     'Ride Cymbal 1', #51
			     'Chinese Cymbal', #52
			     'Ride Bell', #53
			     'Tambourine', #54
			     'Splash Cymbal', #55
			     'Cowbell', #56
			     'Crash Cymbal 2', #57
			     'Vibra-slap', #58
			     'Ride Cymbal 2', #59
			     'High Bongo', #60
			     'Low Bongo', #61
			     'Mute High Conga', #62
			     'Open High Conga', #63
			     'Low Conga', #64
			     'High Timbale', #65
			     'Low Timbale', #66
			     'High Agogo', #67
			     'Low Agogo', #68
			     'Cabasa', #69
			     'Maracas', #70
			     'Short Hi Whistle', #71
			     'Long Low Whistle', #72
			     'Short Guiro', #73
			     'Long Guiro', #74
			     'Claves', #75
			     'High Wood Block', #76
			     'Low Wood Block', #77
			     'Mute Cuica', #78
			     'Open Cuica', #79
			     'Mute Triangle', #80
			     'Open Triangle', #81
			     'Shaker', #82
			     'Jingle Bell', #83
			     'Belltree', #84
			     'Castanets', #85
			     'Mute Surdo', #86
			     'Open Surdo', #87
			     'Applaus2', #88
			     nil, #89
			     nil, #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     '[88] Standard1 Snare1', #97
			     '[88] Standard1 Snare2', #98
			     '[88] Standard2 Snare1', #99
			     '[88] Standard2 Snare2', #100
			     '[55] Snare Drum2', #101
			     'Standard1 Snare1', #102
			     'Standard1 Snare2', #103
			     'Standard Snare3', #104
			     '[88] Jazz Snare1', #105
			     '[88] Jazz Snare2', #106
			     '[88] Room Snare1', #107
			     '[88] Room Snare2', #108
			     '[88] Power Snare1', #109
			     '[88] Power Snare2', #110
			     '[55] Gated Snare', #111
			     '[88] Dance Snare1', #112
			     '[88] Dance Snare2', #113
			     '[88] Disco Snare', #114
			     '[88] Electric Snare2', #115
			     '[55] Electric Snare', #116
			     '[88] Electric Snare 3', #117
			     'TR-707 Snare', #118
			     '[88] TR-808 Snare1', #119
			     '[88] TR-808 Snare2', #120
			     '[88] TR-909 Snare1', #121
			     '[88] TR-909 Snare2', #122
			     'Rap Snare', #123
			     'Jungle Snare1', #124
			     'House Snare1', #125
			     '[88] House Snare', #126
			     'House Snare2', #127
			   ],
	       },
	       {
		   :name => 'HIP HOP',
		   :prog => 10,
		   :list => [
			     '[88] Electric Kick2', #0
			     '[88] Electric Kick1', #1
			     'CR-78 Kick1', #2
			     'CR-78 Kick2', #3
			     'TR-606 Kick1', #4
			     'TR-707 Kick1', #5
			     '[55] TR-808 Kick', #6
			     '[88] TR-808 Kick', #7
			     'TR-808 Kick2', #8
			     '[88] TR-909 Kick', #9
			     '[88] Dance Kick', #10
			     'Hip-Hop Kick2', #11
			     'TR-909 Kick1', #12
			     'Hip-Hop Kick3', #13
			     'Jungle Kick1', #14
			     'Techno Kick1', #15
			     'Bounce Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     'MC-505 Beep1', #22
			     'MC-505 Beep2', #23
			     'Concert SD', #24
			     'Snare Roll', #25
			     'Finger Snap2', #26
			     'High Q', #27
			     'Slap', #28
			     'Scratch Push2', #29
			     'Scratch Pull2', #30
			     'Sticks', #31
			     'Square Click', #32
			     'Metronome Click', #33
			     'Metronome Bell', #34
			     'Hip-Hop Kick2', #35
			     'Hip-Hop Kick1', #36
			     'TR-808 Rim Shot', #37
			     'Rap Snare', #38
			     'Hand Clap', #39
			     'Hip-Hop Snare2', #40
			     'TR-909 Low Tom2', #41
			     'Closed Hi-Hat3', #42
			     'TR-909 Low Tom1', #43
			     'Room Pedal Hi-Hat', #44
			     'TR-909 Mid Tom2', #45
			     'Open Hi-Hat3', #46
			     'TR-909 Mid Tom1', #47
			     'TR-909 High Tom2', #48
			     'TR-909 Crash Cymbal', #49
			     'TR-909 High Tom1', #50
			     'Ride Cymbal1', #51
			     'Reverse Cymbal', #52
			     'Ride Bell', #53
			     'Shake Tambourine', #54
			     'Splash Cymbal', #55
			     'TR-808 Cowbell', #56
			     'Crash Cymbal2', #57
			     'Vibra-slap', #58
			     'Ride Cymbal2', #59
			     'High Bongo', #60
			     'Low Bongo', #61
			     'Mute High Conga', #62
			     'Open High Conga', #63
			     'Open Low Conga', #64
			     'High Timbale', #65
			     'Low Timbale', #66
			     'High Agogo', #67
			     'Low Agogo', #68
			     'Cabasa', #69
			     'TR-808 Maracas', #70
			     'Short High Whisle', #71
			     'Long Low Whisle', #72
			     'Short Guiro', #73
			     'CR-78 Guiro', #74
			     'TR-808 Claves', #75
			     'High Wood Block', #76
			     'Low Wood Block', #77
			     'High Hoo', #78
			     'Low Hoo', #79
			     'Electric Mute Triangle', #80
			     'Electric Open Triangle', #81
			     'TR-626 Shaker', #82
			     'Jingle Bell', #83
			     'Bell Tree', #84
			     'Castanets', #85
			     'Mute Surdo', #86
			     'Open Surdo', #87
			     'Small Club1', #88
			     nil, #89
			     nil, #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     'Techno Hit', #97
			     'Philly Hit', #98
			     'Shock Wave', #99
			     'Lo-Fi Rave', #100
			     'Bam Hit', #101
			     'Bim Hit', #102
			     'Tape Rewind', #103
			     'Phonograph Noise', #104
			     '[88] Power Snare1', #105
			     '[88] Dance Snare1', #106
			     '[88] Dance Snare2', #107
			     '[88] Disco Snare', #108
			     '[88] Electric Snare2', #109
			     '[55] Electric Snare', #110
			     '[88] Electric Snare3', #111
			     'TR-606 Snare', #112
			     'TR-707 Snare', #113
			     '[88] TR-808 Snare1', #114
			     '[88] TR-808 Snare2', #115
			     'TR-808 Snare2', #116
			     '[88] TR-909 Snare1', #117
			     '[88] TR-909 Snare2', #118
			     'TR-909 Snare1', #119
			     'TR-909 Snare2', #120
			     'Rap Snare', #121
			     'Jungle Snare1', #122
			     'House Snare1', #123
			     '[88] House Snare', #124
			     'House Snare2', #125
			     'Voice Tah', #126
			     '[88] Slappy', #127
			   ],
	       },
	       {
		   :name => 'JUNGLE',
		   :prog => 11,
		   :list => [
			     '[88] Electric Kick2', #0
			     '[88] Electric Kick1', #1
			     'CR-78 Kick1', #2
			     'CR-78 Kick2', #3
			     'TR-606 Kick1', #4
			     'TR-707 Kick1', #5
			     '[55] TR-808 Kick', #6
			     '[88] TR-808 Kick', #7
			     'TR-808 Kick2', #8
			     '[88] TR-909 Kick', #9
			     '[88] Dance Kick', #10
			     'Hip-Hop Kick2', #11
			     'TR-909 Kick1', #12
			     'Hip-Hop Kick3', #13
			     'Jungle Kick1', #14
			     'Techno Kick1', #15
			     'Bounce Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     'MC-505 Beep1', #22
			     'MC-505 Beep2', #23
			     'Concert SD', #24
			     'Snare Roll', #25
			     'Finger Snap2', #26
			     'High Q', #27
			     'Slap', #28
			     'Scratch Push2', #29
			     'Scratch Pull2', #30
			     'Sticks', #31
			     'Square Click', #32
			     'Metronome Click', #33
			     'Metronome Bell', #34
			     'Jungle Kick2', #35
			     'Jungle Kick1', #36
			     'Side Stick', #37
			     'Jungle Snare1', #38
			     'Hand Clap2', #39
			     'Jungle Snare2', #40
			     'TR-909 Low Tom2', #41
			     'TR-606 Closed Hi-Hat', #42
			     'TR-909 Low Tom1', #43
			     'Jungle Hi-Hat', #44
			     'TR-909 Mid Tom2', #45
			     'TR-606 Open Hi-Hat', #46
			     'TR-909 Mid Tom1', #47
			     'TR-909 High Tom2', #48
			     'TR-808 Crash Cymbal', #49
			     'TR-909 High Tom1', #50
			     'Ride Cymbal1', #51
			     'Reverse Cymbal', #52
			     'Ride Bell', #53
			     'Shake Tambourine', #54
			     'Splash Cymbal', #55
			     'TR-808 Cowbell', #56
			     'Crash Cymbal2', #57
			     'Vibra-slap', #58
			     'Ride Cymbal2', #59
			     'High Bongo', #60
			     'Low Bongo', #61
			     'Mute High Conga', #62
			     'Open High Conga', #63
			     'Open Low Conga', #64
			     'High Timbale', #65
			     'Low Timbale', #66
			     'High Agogo', #67
			     'Low Agogo', #68
			     'Cabasa', #69
			     'TR-808 Maracas', #70
			     'Short High Whisle', #71
			     'Long Low Whisle', #72
			     'Short Guiro', #73
			     'CR-78 Guiro', #74
			     'TR-808 Claves', #75
			     'High Wood Block', #76
			     'Low Wood Block', #77
			     'High Hoo', #78
			     'Low Hoo', #79
			     'Electric Mute Triangle', #80
			     'Electric Open Triangle', #81
			     'TR-626 Shaker', #82
			     'Jingle Bell', #83
			     'Bell Tree', #84
			     'Castanets', #85
			     'Mute Surdo', #86
			     'Open Surdo', #87
			     'Small Club1', #88
			     nil, #89
			     nil, #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     'Techno Hit', #97
			     'Philly Hit', #98
			     'Shock Wave', #99
			     'Lo-Fi Rave', #100
			     'Bam Hit', #101
			     'Bim Hit', #102
			     'Tape Rewind', #103
			     'Phonograph Noise', #104
			     '[88] Power Snare1', #105
			     '[88] Dance Snare1', #106
			     '[88] Dance Snare2', #107
			     '[88] Disco Snare', #108
			     '[88] Electric Snare2', #109
			     '[55] Electric Snare', #110
			     '[88] Electric Snare3', #111
			     'TR-606 Snare', #112
			     'TR-707 Snare', #113
			     '[88] TR-808 Snare1', #114
			     '[88] TR-808 Snare2', #115
			     'TR-808 Snare2', #116
			     '[88] TR-909 Snare1', #117
			     '[88] TR-909 Snare2', #118
			     'TR-909 Snare1', #119
			     'TR-909 Snare2', #120
			     'Rap Snare', #121
			     'Jungle Snare1', #122
			     'House Snare1', #123
			     '[88] House Snare', #124
			     'House Snare2', #125
			     'Voice Tah', #126
			     '[88] Slappy', #127
			   ],
	       },
	       {
		   :name => 'TECHNO',
		   :prog => 12,
		   :list => [
			     '[88] Electric Kick2', #0
			     '[88] Electric Kick1', #1
			     'CR-78 Kick1', #2
			     'CR-78 Kick2', #3
			     'TR-606 Kick1', #4
			     'TR-707 Kick1', #5
			     '[55] TR-808 Kick', #6
			     '[88] TR-808 Kick', #7
			     'TR-808 Kick2', #8
			     '[88] TR-909 Kick', #9
			     '[88] Dance Kick', #10
			     'Hip-Hop Kick2', #11
			     'TR-909 Kick1', #12
			     'Hip-Hop Kick3', #13
			     'Jungle Kick1', #14
			     'Techno Kick1', #15
			     'Bounce Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     'MC-505 Beep1', #22
			     'MC-505 Beep2', #23
			     'Concert SD', #24
			     'Snare Roll', #25
			     'Finger Snap2', #26
			     'High Q', #27
			     'Slap', #28
			     'Scratch Push2', #29
			     'Scratch Pull2', #30
			     'Sticks', #31
			     'Square Click', #32
			     'Metronome Click', #33
			     'Metronome Bell', #34
			     'Techno Kick2', #35
			     'Techno Kick1', #36
			     'TR-808 Rim Shot', #37
			     'Techno Snare1', #38
			     'TR-707 Hand Clap', #39
			     'Techno Snare2', #40
			     'TR-808 Low Tom2', #41
			     'TR-707 Closed Hi-Hat', #42
			     'TR-808 Low Tom1', #43
			     'CR-78 Closed Hi-Hat', #44
			     'TR-808 Mid Tom2', #45
			     'TR-909 Open Hi-Hat', #46
			     'TR-808 Mid Tom1', #47
			     'TR-808 High Tom2', #48
			     'TR-909 Crash Cymbal', #49
			     'TR-808 High Tom1', #50
			     'Ride Cymbal1', #51
			     'Reverse Cymbal', #52
			     'Ride Bell', #53
			     'Shake Tambourine', #54
			     'Splash Cymbal', #55
			     'TR-808 Cowbell', #56
			     'TR-909 Crash Cymbal', #57
			     'Vibra-slap', #58
			     'Ride Cymbal2', #59
			     'CR-78 High Bongo', #60
			     'CR-78 Low Bongo', #61
			     'TR-808 Mute High Conga', #62
			     'TR-808 Open High Conga', #63
			     'TR-808 Open Low Conga', #64
			     'High Timbale', #65
			     'Low Timbale', #66
			     'High Agogo', #67
			     'Low Agogo', #68
			     'Cabasa', #69
			     'TR-808 Maracas', #70
			     'Short High Whisle', #71
			     'Long Low Whisle', #72
			     'Short Guiro', #73
			     'CR-78 Guiro', #74
			     'TR-808 Claves', #75
			     'High Wood Block', #76
			     'Low Wood Block', #77
			     'High Hoo', #78
			     'Low Hoo', #79
			     'Electric Mute Triangle', #80
			     'Electric Open Triangle', #81
			     'TR-626 Shaker', #82
			     'Jingle Bell', #83
			     'Bell Tree', #84
			     'Castanets', #85
			     'Mute Surdo', #86
			     'Open Surdo', #87
			     'Applaus2', #88
			     nil, #89
			     nil, #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     'Techno Hit', #97
			     'Philly Hit', #98
			     'Shock Wave', #99
			     'Lo-Fi Rave', #100
			     'Bam Hit', #101
			     'Bim Hit', #102
			     'Tape Rewind', #103
			     'Phonograph Noise', #104
			     '[88] Power Snare1', #105
			     '[88] Dance Snare1', #106
			     '[88] Dance Snare2', #107
			     '[88] Disco Snare', #108
			     '[88] Electric Snare2', #109
			     '[55] Electric Snare', #110
			     '[88] Electric Snare3', #111
			     'TR-606 Snare', #112
			     'TR-707 Snare', #113
			     '[88] TR-808 Snare1', #114
			     '[88] TR-808 Snare2', #115
			     'TR-808 Snare2', #116
			     '[88] TR-909 Snare1', #117
			     '[88] TR-909 Snare2', #118
			     'TR-909 Snare1', #119
			     'TR-909 Snare2', #120
			     'Rap Snare', #121
			     'Jungle Snare1', #122
			     'House Snare1', #123
			     '[88] House Snare', #124
			     'House Snare2', #125
			     'Voice Tah', #126
			     '[88] Slappy', #127
			   ],
	       },
	       {
		   :name => 'POWER',
		   :prog => 17,
		   :list => [
			     '[88] Standard1 Kick1', #0
			     '[88] Standard1 Kick2', #1
			     '[88] Standard2 Kick1', #2
			     '[88] Standard2 Kick2', #3
			     '[55] Kick Drum1', #4
			     '[55] Kick Drum2', #5
			     '[88] Jazz Kick1', #6
			     '[88] Jazz Kick2', #7
			     '[88] Room Kick1', #8
			     '[88] Room Kick2', #9
			     '[88] Power Kick1', #10
			     '[88] Power Kick2', #11
			     '[88] Electric Kick2', #12
			     '[88] Electric Kick1', #13
			     '[88] TR-808 Kick', #14
			     '[88] TR-909 Kick', #15
			     '[88] Dance Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     'MC-505 Beep1', #22
			     'MC-505 Beep2', #23
			     'Concert SD', #24
			     'Snare Roll', #25
			     'Finger Snap2', #26
			     'High Q', #27
			     'Slap', #28
			     'Scratch Push', #29
			     'Scratch Pull', #30
			     'Sticks', #31
			     'Square Click', #32
			     'Metronome Click', #33
			     'Metronome Bell', #34
			     'Kick Drum 2', #35
			     'MONDO Kick', #36
			     'Side Stick', #37
			     'Gated SD', #38
			     'Hand Clap', #39
			     'Snare Drum 2', #40
			     'Room Low Tom 2', #41
			     'Closed Hi-Hat', #42
			     'Room Low Tom 1', #43
			     'Pedal Hi-Hat', #44
			     'Room Mid Tom 2', #45
			     'Open Hi-Hat', #46
			     'Room Mid Tom 1', #47
			     'Room Hi Tom 2', #48
			     'Crash Cymbal 1', #49
			     'Room Hi Tom 1', #50
			     'Ride Cymbal 1', #51
			     'Chinese Cymbal', #52
			     'Ride Bell', #53
			     'Tambourine', #54
			     'Splash Cymbal', #55
			     'Cowbell', #56
			     'Crash Cymbal 2', #57
			     'Vibra-slap', #58
			     'Ride Cymbal 2', #59
			     'High Bongo', #60
			     'Low Bongo', #61
			     'Mute High Conga', #62
			     'Open High Conga', #63
			     'Low Conga', #64
			     'High Timbale', #65
			     'Low Timbale', #66
			     'High Agogo', #67
			     'Low Agogo', #68
			     'Cabasa', #69
			     'Maracas', #70
			     'Short Hi Whistle', #71
			     'Long Low Whistle', #72
			     'Short Guiro', #73
			     'Long Guiro', #74
			     'Claves', #75
			     'High Wood Block', #76
			     'Low Wood Block', #77
			     'Mute Cuica', #78
			     'Open Cuica', #79
			     'Mute Triangle', #80
			     'Open Triangle', #81
			     'Shaker', #82
			     'Jingle Bell', #83
			     'Belltree', #84
			     'Castanets', #85
			     'Mute Surdo', #86
			     'Open Surdo', #87
			     'Applaus2', #88
			     nil, #89
			     nil, #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     '[88] Standard1 Snare1', #97
			     '[88] Standard1 Snare2', #98
			     '[88] Standard2 Snare1', #99
			     '[88] Standard2 Snare2', #100
			     '[55] Snare Drum2', #101
			     'Standard1 Snare1', #102
			     'Standard1 Snare2', #103
			     'Standard Snare3', #104
			     '[88] Jazz Snare1', #105
			     '[88] Jazz Snare2', #106
			     '[88] Room Snare1', #107
			     '[88] Room Snare2', #108
			     '[88] Power Snare1', #109
			     '[88] Power Snare2', #110
			     '[55] Gated Snare', #111
			     '[88] Dance Snare1', #112
			     '[88] Dance Snare2', #113
			     '[88] Disco Snare', #114
			     '[88] Electric Snare2', #115
			     '[55] Electric Snare', #116
			     '[88] Electric Snare 3', #117
			     'TR-707 Snare', #118
			     '[88] TR-808 Snare1', #119
			     '[88] TR-808 Snare2', #120
			     '[88] TR-909 Snare1', #121
			     '[88] TR-909 Snare2', #122
			     'Rap Snare', #123
			     'Jungle Snare1', #124
			     'House Snare1', #125
			     '[88] House Snare', #126
			     'House Snare2', #127
			   ],
	       },
	       {
		   :name => 'ELECTRONIC',
		   :prog => 25,
		   :list => [
			     '[88] Electric Kick2', #0
			     '[88] Electric Kick1', #1
			     'CR-78 Kick1', #2
			     'CR-78 Kick2', #3
			     'TR-606 Kick1', #4
			     'TR-707 Kick1', #5
			     '[55] TR-808 Kick', #6
			     '[88] TR-808 Kick', #7
			     'TR-808 Kick2', #8
			     '[88] TR-909 Kick', #9
			     '[88] Dance Kick', #10
			     'Hip-Hop Kick2', #11
			     'TR-909 Kick1', #12
			     'Hip-Hop Kick3', #13
			     'Jungle Kick1', #14
			     'Techno Kick1', #15
			     'Bounce Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     'MC-505 Beep1', #24
			     'MC-505 Beep2', #25
			     'Concert SD', #26
			     'Snare Roll', #27
			     'Finger Snap', #28
			     'High Q', #29
			     'Slap', #30
			     'Scratch Push', #31
			     'Scratch Pull', #32
			     'Sticks', #33
			     'Square Click', #34
			     'Metronome Click', #35
			     'Metronome Bell', #36
			     'Kick Drum 2', #37
			     'Elec BD', #38
			     'Side Stick', #39
			     'Elec SD', #40
			     'Hand Clap', #41
			     'Gated SD', #42
			     'Elec Low Tom 2', #43
			     'Closed Hi-Hat', #44
			     'Elec Low Tom 1', #45
			     'Pedal Hi-Hat', #46
			     'Elec Mid Tom 2', #47
			     'Open Hi-Hat', #48
			     'Elec Mid Tom 1', #49
			     'Elec Hi Tom 2', #50
			     'Crash Cymbal 1', #51
			     'Elec Hi Tom 1', #52
			     'Ride Cymbal 1', #53
			     'Reverse Cymbal', #54
			     'Ride Bell', #55
			     'Tambourine', #56
			     'Splash Cymbal', #57
			     'Cowbell', #58
			     'Crash Cymbal 2', #59
			     'Vibra-slap', #60
			     'Ride Cymbal 2', #61
			     'High Bongo', #62
			     'Low Bongo', #63
			     'Mute High Conga', #64
			     'Open High Conga', #65
			     'Low Conga', #66
			     'High Timbale', #67
			     'Low Timbale', #68
			     'High Agogo', #69
			     'Low Agogo', #70
			     'Cabasa', #71
			     'Maracas', #72
			     'Short Hi Whistle', #73
			     'Long Low Whistle', #74
			     'Short Guiro', #75
			     'Long Guiro', #76
			     'Claves', #77
			     'High Wood Block', #78
			     'Low Wood Block', #79
			     'Mute Cuica', #80
			     'Open Cuica', #81
			     'Mute Triangle', #82
			     'Open Triangle', #83
			     'Shaker', #84
			     'Jingle Bell', #85
			     'Belltree', #86
			     'Castanets', #87
			     'Mute Surdo', #88
			     'Open Surdo', #89
			     'Small Club1', #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     'Techno Hit', #97
			     'Philly Hit', #98
			     'Shock Wave', #99
			     'Lo-Fi Rave', #100
			     'Bam Hit', #101
			     'Bim Hit', #102
			     'Tape Rewind', #103
			     'Phonograph Noise', #104
			     '[88] Power Snare1', #105
			     '[88] Dance Snare1', #106
			     '[88] Dance Snare2', #107
			     '[88] Disco Snare', #108
			     '[88] Electric Snare2', #109
			     '[55] Electric Snare', #110
			     '[88] Electric Snare3', #111
			     'TR-606 Snare', #112
			     'TR-707 Snare', #113
			     '[88] TR-808 Snare1', #114
			     '[88] TR-808 Snare2', #115
			     'TR-808 Snare2', #116
			     '[88] TR-909 Snare1', #117
			     '[88] TR-909 Snare2', #118
			     'TR-909 Snare1', #119
			     'TR-909 Snare2', #120
			     'Rap Snare', #121
			     'Jungle Snare1', #122
			     'House Snare1', #123
			     '[88] House Snare', #124
			     'House Snare2', #125
			     'Voice Tah', #126
			     '[88] Slappy', #127
			   ],
	       },
	       {
		   :name => 'TR-808',
		   :prog => 26,
		   :list => [
			     '[88] Electric Kick2', #0
			     '[88] Electric Kick1', #1
			     'CR-78 Kick1', #2
			     'CR-78 Kick2', #3
			     'TR-606 Kick1', #4
			     'TR-707 Kick1', #5
			     '[55] TR-808 Kick', #6
			     '[88] TR-808 Kick', #7
			     'TR-808 Kick2', #8
			     '[88] TR-909 Kick', #9
			     '[88] Dance Kick', #10
			     'Hip-Hop Kick2', #11
			     'TR-909 Kick1', #12
			     'Hip-Hop Kick3', #13
			     'Jungle Kick1', #14
			     'Techno Kick1', #15
			     'Bounce Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     'MC-505 Beep1', #24
			     'MC-505 Beep2', #25
			     'Concert SD', #26
			     'Snare Roll', #27
			     'Finger Snap2', #28
			     'High Q', #29
			     'Slap', #30
			     'Scratch Push', #31
			     'Scratch Pull', #32
			     'Sticks', #33
			     'Square Click', #34
			     'Metronome Click', #35
			     'Metronome Bell', #36
			     'Kick Drum 2', #37
			     '808 Bass Drum', #38
			     '808 Rim Shot', #39
			     '808 Snare Drum', #40
			     'Hand Clap', #41
			     'Snare Drum 2', #42
			     '808 Low Tom 2', #43
			     '808 CHH', #44
			     '808 Low Tom 1', #45
			     '808 CHH', #46
			     '808 Mid Tom 2', #47
			     '808 OHH', #48
			     '808 Mid Tom 1', #49
			     '808 Hi Tom 2', #50
			     '808 Cymbal', #51
			     '808 Hi Tom 1', #52
			     'Ride Cymbal 1', #53
			     'Chinese Cymbal', #54
			     'Ride Bell', #55
			     'Tambourine', #56
			     'Splash Cymbal', #57
			     '808 Cowbell', #58
			     'Crash Cymbal 2', #59
			     'Vibra-slap', #60
			     'Ride Cymbal 2', #61
			     'High Bongo', #62
			     'Low Bongo', #63
			     '808 High Conga', #64
			     '808 Mid Conga', #65
			     '808 Low Conga', #66
			     'High Timbale', #67
			     'Low Timbale', #68
			     'High Agogo', #69
			     'Low Agogo', #70
			     'Cabasa', #71
			     '808 Maracas', #72
			     'Short Hi Whistle', #73
			     'Long Low Whistle', #74
			     'Short Guiro', #75
			     'Long Guiro', #76
			     '808 Claves', #77
			     'High Wood Block', #78
			     'Low Wood Block', #79
			     'Mute Cuica', #80
			     'Open Cuica', #81
			     'Mute Triangle', #82
			     'Open Triangle', #83
			     'Shaker', #84
			     'Jingle Bell', #85
			     'Belltree', #86
			     'Castanets', #87
			     'Mute Surdo', #88
			     'Open Surdo', #89
			     'Small Club1', #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     'Techno Hit', #97
			     'Philly Hit', #98
			     'Shock Wave', #99
			     'Lo-Fi Rave', #100
			     'Bam Hit', #101
			     'Bim Hit', #102
			     'Tape Rewind', #103
			     'Phonograph Noise', #104
			     '[88] Power Snare1', #105
			     '[88] Dance Snare1', #106
			     '[88] Dance Snare2', #107
			     '[88] Disco Snare', #108
			     '[88] Electric Snare2', #109
			     '[55] Electric Snare', #110
			     '[88] Electric Snare3', #111
			     'TR-606 Snare', #112
			     'TR-707 Snare', #113
			     '[88] TR-808 Snare1', #114
			     '[88] TR-808 Snare2', #115
			     'TR-808 Snare2', #116
			     '[88] TR-909 Snare1', #117
			     '[88] TR-909 Snare2', #118
			     'TR-909 Snare1', #119
			     'TR-909 Snare2', #120
			     'Rap Snare', #121
			     'Jungle Snare1', #122
			     'House Snare1', #123
			     '[88] House Snare', #124
			     'House Snare2', #125
			     'Voice Tah', #126
			     '[88] Slappy', #127
			   ],
	       },
	       {
		   :name => 'DANCE',
		   :prog => 27,
		   :list => [
			     '[88] Electric Kick2', #0
			     '[88] Electric Kick1', #1
			     'CR-78 Kick1', #2
			     'CR-78 Kick2', #3
			     'TR-606 Kick1', #4
			     'TR-707 Kick1', #5
			     '[55] TR-808 Kick', #6
			     '[88] TR-808 Kick', #7
			     'TR-808 Kick2', #8
			     '[88] TR-909 Kick', #9
			     '[88] Dance Kick', #10
			     'Hip-Hop Kick2', #11
			     'TR-909 Kick1', #12
			     'Hip-Hop Kick3', #13
			     'Jungle Kick1', #14
			     'Techno Kick1', #15
			     'Bounce Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     'MC-505 Beep1', #24
			     'MC-505 Beep2', #25
			     'Concert SD', #26
			     'Snare Roll', #27
			     'Finger Snap', #28
			     'High Q', #29
			     'Slap', #30
			     'Scratch Push2', #31
			     'Scratch Pull2', #32
			     'Sticks', #33
			     'Square Click', #34
			     'Metronome Click', #35
			     'Metronome Bell', #36
			     'TR-909 Kick1', #37
			     'Electric Kick2', #38
			     'Side Stick', #39
			     'House Snare1', #40
			     'Hand Clap', #41
			     'Dance Snare2', #42
			     'Electric Low Tom2', #43
			     'CR-78 Closed Hi-Hat', #44
			     'Electric Low Tom1', #45
			     'TR-808 Closed Hi-Hat2', #46
			     'Electric Mid Tom2', #47
			     'CR-78 Open Hi-Hat', #48
			     'Electric Mid Tom1', #49
			     'Electric High Tom2', #50
			     'TR-808 Crash Cymbal', #51
			     'Electric High Tom1', #52
			     'TR-606 Ride Cymbal', #53
			     'Reverse Cymbal', #54
			     'Ride Bell', #55
			     'Shake Tambourine', #56
			     'Splash Cymbal', #57
			     'TR-808 Cowbell', #58
			     'Crash Cymbal2', #59
			     'Vibra-slap', #60
			     'Ride Cymbal2', #61
			     'High Bongo', #62
			     'Low Bongo', #63
			     'Mute High Conga', #64
			     'Open High Conga', #65
			     'Open Low Conga', #66
			     'High Timbale', #67
			     'Low Timbale', #68
			     'High Agogo', #69
			     'Low Agogo', #70
			     'Cabasa', #71
			     'Maracas', #72
			     'Short High Whisle', #73
			     'Long Low Whisle', #74
			     'Short Guiro', #75
			     'Long Guiro', #76
			     'Claves', #77
			     'High Wood Block', #78
			     'Low Wood Block', #79
			     'High Hoo', #80
			     'Low Hoo', #81
			     'Electric Mute Triangle', #82
			     'Electric Open Triangle', #83
			     'TR-626 Shaker', #84
			     'Jingle Bell', #85
			     'Bell Tree', #86
			     'Castanets', #87
			     'Mute Surdo', #88
			     'Open Surdo', #89
			     'Small Club1', #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     'Techno Hit', #97
			     'Philly Hit', #98
			     'Shock Wave', #99
			     'Lo-Fi Rave', #100
			     'Bam Hit', #101
			     'Bim Hit', #102
			     'Tape Rewind', #103
			     'Phonograph Noise', #104
			     '[88] Power Snare1', #105
			     '[88] Dance Snare1', #106
			     '[88] Dance Snare2', #107
			     '[88] Disco Snare', #108
			     '[88] Electric Snare2', #109
			     '[55] Electric Snare', #110
			     '[88] Electric Snare3', #111
			     'TR-606 Snare', #112
			     'TR-707 Snare', #113
			     '[88] TR-808 Snare1', #114
			     '[88] TR-808 Snare2', #115
			     'TR-808 Snare2', #116
			     '[88] TR-909 Snare1', #117
			     '[88] TR-909 Snare2', #118
			     'TR-909 Snare1', #119
			     'TR-909 Snare2', #120
			     'Rap Snare', #121
			     'Jungle Snare1', #122
			     'House Snare1', #123
			     '[88] House Snare', #124
			     'House Snare2', #125
			     'Voice Tah', #126
			     '[88] Slappy', #127
			   ],
	       },
	       {
		   :name => 'CR-78',
		   :prog => 28,
		   :list => [
			     '[88] Electric Kick2', #0
			     '[88] Electric Kick1', #1
			     'CR-78 Kick1', #2
			     'CR-78 Kick2', #3
			     'TR-606 Kick1', #4
			     'TR-707 Kick1', #5
			     '[55] TR-808 Kick', #6
			     '[88] TR-808 Kick', #7
			     'TR-808 Kick2', #8
			     '[88] TR-909 Kick', #9
			     '[88] Dance Kick', #10
			     'Hip-Hop Kick2', #11
			     'TR-909 Kick1', #12
			     'Hip-Hop Kick3', #13
			     'Jungle Kick1', #14
			     'Techno Kick1', #15
			     'Bounce Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     'MC-505 Beep1', #24
			     'MC-505 Beep2', #25
			     'Concert SD', #26
			     'Snare Roll', #27
			     'Finger Snap2', #28
			     'High Q', #29
			     'Slap', #30
			     'Scratch Push2', #31
			     'Scratch Pull2', #32
			     'Sticks', #33
			     'Square Click', #34
			     'Metronome Click', #35
			     'Metronome Bell', #36
			     'CR-78 Kick2', #37
			     'CR-78 Kick1', #38
			     'CR-78 Rim Shot', #39
			     'CR-78 Snare1', #40
			     'TR-707 Hand Clap', #41
			     'CR-78 Snare2', #42
			     'CR-78 Low Tom2', #43
			     'CR-78 Closed Hi-Hat', #44
			     'CR-78 Low Tom1', #45
			     'TR-606 Closed Hi-Hat', #46
			     'CR-78 Mid Tom2', #47
			     'CR-78 Open Hi-Hat', #48
			     'CR-78 Mid Tom1', #49
			     'CR-78 High Tom2', #50
			     'TR-808 Crash Cymbal', #51
			     'CR-78 High Tom1', #52
			     'TR-606 Ride Cymbal', #53
			     'Chinese Cymbal', #54
			     'Ride Bell', #55
			     'CR-78 Tambourine', #56
			     'Splash Cymbal', #57
			     'CR-78 Cowbell', #58
			     'TR-909 Crash Cymbal', #59
			     'Vibra-slap', #60
			     'Ride Cymbal Edge', #61
			     'CR-78 High Bongo', #62
			     'CR-78 Low Bongo', #63
			     'TR-808 Mute High Conga', #64
			     'TR-808 Open High Conga', #65
			     'TR-808 Open Low Conga', #66
			     'High Timbale', #67
			     'Low Timbale', #68
			     'High Agogo', #69
			     'Low Agogo', #70
			     'Cabasa', #71
			     'CR-78 Maracas', #72
			     'Short Hi Whistle', #73
			     'Long Low Whistle', #74
			     'Short Guiro', #75
			     'CR-78 Guiro', #76
			     'CR-78 Claves', #77
			     'High Wood Block', #78
			     'Low Wood Block', #79
			     'High Hoo', #80
			     'Low Hoo', #81
			     'CR-78 Low Beat', #82
			     'CR-78 High Beat', #83
			     'TR-626 Shaker', #84
			     'Jingle Bell', #85
			     'Belltree', #86
			     'Castanets', #87
			     'Mute Surdo', #88
			     'Open Surdo', #89
			     'Small Club1', #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     'Techno Hit', #97
			     'Philly Hit', #98
			     'Shock Wave', #99
			     'Lo-Fi Rave', #100
			     'Bam Hit', #101
			     'Bim Hit', #102
			     'Tape Rewind', #103
			     'Phonograph Noise', #104
			     '[88] Power Snare1', #105
			     '[88] Dance Snare1', #106
			     '[88] Dance Snare2', #107
			     '[88] Disco Snare', #108
			     '[88] Electric Snare2', #109
			     '[55] Electric Snare', #110
			     '[88] Electric Snare3', #111
			     'TR-606 Snare', #112
			     'TR-707 Snare', #113
			     '[88] TR-808 Snare1', #114
			     '[88] TR-808 Snare2', #115
			     'TR-808 Snare2', #116
			     '[88] TR-909 Snare1', #117
			     '[88] TR-909 Snare2', #118
			     'TR-909 Snare1', #119
			     'TR-909 Snare2', #120
			     'Rap Snare', #121
			     'Jungle Snare1', #122
			     'House Snare1', #123
			     '[88] House Snare', #124
			     'House Snare2', #125
			     'Voice Tah', #126
			     '[88] Slappy', #127
			   ],
	       },
	       {
		   :name => 'TR-606',
		   :prog => 29,
		   :list => [
			     '[88] Electric Kick2', #0
			     '[88] Electric Kick1', #1
			     'CR-78 Kick1', #2
			     'CR-78 Kick2', #3
			     'TR-606 Kick1', #4
			     'TR-707 Kick1', #5
			     '[55] TR-808 Kick', #6
			     '[88] TR-808 Kick', #7
			     'TR-808 Kick2', #8
			     '[88] TR-909 Kick', #9
			     '[88] Dance Kick', #10
			     'Hip-Hop Kick2', #11
			     'TR-909 Kick1', #12
			     'Hip-Hop Kick3', #13
			     'Jungle Kick1', #14
			     'Techno Kick1', #15
			     'Bounce Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     'MC-505 Beep1', #24
			     'MC-505 Beep2', #25
			     'Concert SD', #26
			     'Snare Roll', #27
			     'Finger Snap2', #28
			     'High Q', #29
			     'Slap', #30
			     'Scratch Push2', #31
			     'Scratch Pull2', #32
			     'Sticks', #33
			     'Square Click', #34
			     'Metronome Click', #35
			     'Metronome Bell', #36
			     'CR-78 Kick2', #37
			     'TR-606 Kick1', #38
			     'CR-78 Rim Shot', #39
			     'TR-606 Snare1', #40
			     'TR-707 Hand Clap', #41
			     'TR-606 Snare2', #42
			     'TR-606 Low Tom2', #43
			     'TR-606 Closed Hi-Hat', #44
			     'TR-606 Low Tom1', #45
			     'TR-606 Closed Hi-Hat', #46
			     'TR-606 Mid Tom2', #47
			     'TR-606 Open Hi-Hat', #48
			     'TR-606 Mid Tom1', #49
			     'TR-606 High Tom2', #50
			     'TR-808 Crash Cymbal', #51
			     'TR-606 High Tom1', #52
			     'TR-606 Ride Cymbal', #53
			     'Chinese Cymbal', #54
			     'Ride Bell', #55
			     'CR-78 Tambourine', #56
			     'Splash Cymbal', #57
			     'CR-78 Cowbell', #58
			     'TR-909 Crash Cymbal', #59
			     'Vibra-slap', #60
			     'Ride Cymbal Edge', #61
			     'CR-78 High Bongo', #62
			     'CR-78 Low Bongo', #63
			     'TR-808 Mute High Conga', #64
			     'TR-808 Open High Conga', #65
			     'TR-808 Open Low Conga', #66
			     'High Timbale', #67
			     'Low Timbale', #68
			     'High Agogo', #69
			     'Low Agogo', #70
			     'Cabasa', #71
			     'CR-78 Maracas', #72
			     'Short High Whisle', #73
			     'Long Low Whisle', #74
			     'Short Guiro', #75
			     'CR-78 Guiro', #76
			     'CR-78 Claves', #77
			     'High Wood Block', #78
			     'Low Wood Block', #79
			     'High Hoo', #80
			     'Low Hoo', #81
			     'CR-78 Low Beat', #82
			     'CR-78 High Beat', #83
			     'TR-626 Shaker', #84
			     'Jingle Bell', #85
			     'Bell Tree', #86
			     'Castanets', #87
			     'Mute Surdo', #88
			     'Open Surdo', #89
			     'Small Club1', #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     'Techno Hit', #97
			     'Philly Hit', #98
			     'Shock Wave', #99
			     'Lo-Fi Rave', #100
			     'Bam Hit', #101
			     'Bim Hit', #102
			     'Tape Rewind', #103
			     'Phonograph Noise', #104
			     '[88] Power Snare1', #105
			     '[88] Dance Snare1', #106
			     '[88] Dance Snare2', #107
			     '[88] Disco Snare', #108
			     '[88] Electric Snare2', #109
			     '[55] Electric Snare', #110
			     '[88] Electric Snare3', #111
			     'TR-606 Snare', #112
			     'TR-707 Snare', #113
			     '[88] TR-808 Snare1', #114
			     '[88] TR-808 Snare2', #115
			     'TR-808 Snare2', #116
			     '[88] TR-909 Snare1', #117
			     '[88] TR-909 Snare2', #118
			     'TR-909 Snare1', #119
			     'TR-909 Snare2', #120
			     'Rap Snare', #121
			     'Jungle Snare1', #122
			     'House Snare1', #123
			     '[88] House Snare', #124
			     'House Snare2', #125
			     'Voice Tah', #126
			     '[88] Slappy', #127
			   ],
	       },
	       {
		   :name => 'TR-707',
		   :prog => 30,
		   :list => [
			     '[88] Electric Kick2', #0
			     '[88] Electric Kick1', #1
			     'CR-78 Kick1', #2
			     'CR-78 Kick2', #3
			     'TR-606 Kick1', #4
			     'TR-707 Kick1', #5
			     '[55] TR-808 Kick', #6
			     '[88] TR-808 Kick', #7
			     'TR-808 Kick2', #8
			     '[88] TR-909 Kick', #9
			     '[88] Dance Kick', #10
			     'Hip-Hop Kick2', #11
			     'TR-909 Kick1', #12
			     'Hip-Hop Kick3', #13
			     'Jungle Kick1', #14
			     'Techno Kick1', #15
			     'Bounce Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     'MC-505 Beep1', #24
			     'MC-505 Beep2', #25
			     'Concert SD', #26
			     'Snare Roll', #27
			     'Finger Snap2', #28
			     'High Q', #29
			     'Slap', #30
			     'Scratch Push2', #31
			     'Scratch Pull2', #32
			     'Sticks', #33
			     'Square Click', #34
			     'Metronome Click', #35
			     'Metronome Bell', #36
			     'TR-707 Kick2', #37
			     'TR-707 Kick1', #38
			     'TR-707 Rim Shot', #39
			     'TR-707 Snare1', #40
			     'TR-707 Hand Clap', #41
			     'TR-707 Snare2', #42
			     'TR-707 Low Tom2', #43
			     'TR-707 Closed Hi-Hat', #44
			     'TR-707 Low Tom1', #45
			     'TR-707 Closed Hi-Hat', #46
			     'TR-707 Mid Tom2', #47
			     'TR-707 Open Hi-Hat', #48
			     'TR-707 Mid Tom1', #49
			     'TR-707 High Tom2', #50
			     'TR-909 Crash Cymbal', #51
			     'TR-707 High Tom1', #52
			     'TR-909 Ride Cymbal', #53
			     'Chinese Cymbal', #54
			     'Ride Bell', #55
			     'Tambourine2', #56
			     'Splash Cymbal', #57
			     'TR-808 Cowbell', #58
			     'Crash Cymbal2', #59
			     'Vibra-slap', #60
			     'Ride Cymbal Edge', #61
			     'High Bongo', #62
			     'Low Bongo', #63
			     'Mute High Conga', #64
			     'Open High Conga', #65
			     'Open Low Conga', #66
			     'High Timbale', #67
			     'Low Timbale', #68
			     'High Agogo', #69
			     'Low Agogo', #70
			     'Cabasa', #71
			     'TR-808 Maracas', #72
			     'Short High Whisle', #73
			     'Long Low Whisle', #74
			     'Short Guiro', #75
			     'Long Guiro', #76
			     'Claves', #77
			     'High Wood Block', #78
			     'Low Wood Block', #79
			     'High Hoo', #80
			     'Low Hoo', #81
			     'Electric Mute Triangle', #82
			     'Electric Open Triangle', #83
			     'TR-626 Shaker', #84
			     'Jingle Bell', #85
			     'Bell Tree', #86
			     'Castanets', #87
			     'Mute Surdo', #88
			     'Open Surdo', #89
			     'Small Club1', #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     'Techno Hit', #97
			     'Philly Hit', #98
			     'Shock Wave', #99
			     'Lo-Fi Rave', #100
			     'Bam Hit', #101
			     'Bim Hit', #102
			     'Tape Rewind', #103
			     'Phonograph Noise', #104
			     '[88] Power Snare1', #105
			     '[88] Dance Snare1', #106
			     '[88] Dance Snare2', #107
			     '[88] Disco Snare', #108
			     '[88] Electric Snare2', #109
			     '[55] Electric Snare', #110
			     '[88] Electric Snare3', #111
			     'TR-606 Snare', #112
			     'TR-707 Snare', #113
			     '[88] TR-808 Snare1', #114
			     '[88] TR-808 Snare2', #115
			     'TR-808 Snare2', #116
			     '[88] TR-909 Snare1', #117
			     '[88] TR-909 Snare2', #118
			     'TR-909 Snare1', #119
			     'TR-909 Snare2', #120
			     'Rap Snare', #121
			     'Jungle Snare1', #122
			     'House Snare1', #123
			     '[88] House Snare', #124
			     'House Snare2', #125
			     'Voice Tah', #126
			     '[88] Slappy', #127
			   ],
	       },
	       {
		   :name => 'TR-909',
		   :prog => 31,
		   :list => [
			     '[88] Electric Kick2', #0
			     '[88] Electric Kick1', #1
			     'CR-78 Kick1', #2
			     'CR-78 Kick2', #3
			     'TR-606 Kick1', #4
			     'TR-707 Kick1', #5
			     '[55] TR-808 Kick', #6
			     '[88] TR-808 Kick', #7
			     'TR-808 Kick2', #8
			     '[88] TR-909 Kick', #9
			     '[88] Dance Kick', #10
			     'Hip-Hop Kick2', #11
			     'TR-909 Kick1', #12
			     'Hip-Hop Kick3', #13
			     'Jungle Kick1', #14
			     'Techno Kick1', #15
			     'Bounce Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     'MC-505 Beep1', #24
			     'MC-505 Beep2', #25
			     'Concert SD', #26
			     'Snare Roll', #27
			     'Finger Snap2', #28
			     'High Q', #29
			     'Slap', #30
			     'Scratch Push2', #31
			     'Scratch Pull2', #32
			     'Sticks', #33
			     'Square Click', #34
			     'Metronome Click', #35
			     'Metronome Bell', #36
			     'Techno Kick2', #37
			     'TR-909 Kick1', #38
			     'TR-909 Rim', #39
			     'TR-909 Snare1', #40
			     'Hand Clap', #41
			     'TR-909 Snare2', #42
			     'TR-909 Low Tom2', #43
			     'TR-707 Closed Hi-Hat', #44
			     'TR-909 Low Tom1', #45
			     'TR-707 Closed Hi-Hat', #46
			     'TR-909 Mid Tom2', #47
			     'TR-909 Open Hi-Hat', #48
			     'TR-909 Mid Tom1', #49
			     'TR-909 High Tom2', #50
			     'TR-909 Crash Cymbal', #51
			     'TR-909 High Tom1', #52
			     'TR-909 Ride Cymbal', #53
			     'Chinese Cymbal', #54
			     'Ride Bell', #55
			     'Tambourine2', #56
			     'Splash Cymbal', #57
			     'TR-808 Cowbell', #58
			     'Crash Cymbal2', #59
			     'Vibra-slap', #60
			     'Ride Cymbal Edge', #61
			     'High Bongo', #62
			     'Low Bongo', #63
			     'Mute High Conga', #64
			     'Open High Conga', #65
			     'Open Low Conga', #66
			     'High Timbale', #67
			     'Low Timbale', #68
			     'High Agogo', #69
			     'Low Agogo', #70
			     'Cabasa', #71
			     'TR-808 Maracas', #72
			     'Short High Whisle', #73
			     'Long Low Whisle', #74
			     'Short Guiro', #75
			     'CR-78 Guiro', #76
			     'TR-808 Claves', #77
			     'High Wood Block', #78
			     'Low Wood Block', #79
			     'High Hoo', #80
			     'Low Hoo', #81
			     'Electric Mute Triangle', #82
			     'Electric Open Triangle', #83
			     'TR-626 Shaker', #84
			     'Jingle Bell', #85
			     'Bell Tree', #86
			     'Castanets', #87
			     'Mute Surdo', #88
			     'Open Surdo', #89
			     'Applaus2', #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     'Techno Hit', #97
			     'Philly Hit', #98
			     'Shock Wave', #99
			     'Lo-Fi Rave', #100
			     'Bam Hit', #101
			     'Bim Hit', #102
			     'Tape Rewind', #103
			     'Phonograph Noise', #104
			     '[88] Power Snare1', #105
			     '[88] Dance Snare1', #106
			     '[88] Dance Snare2', #107
			     '[88] Disco Snare', #108
			     '[88] Electric Snare2', #109
			     '[55] Electric Snare', #110
			     '[88] Electric Snare3', #111
			     'TR-606 Snare', #112
			     'TR-707 Snare', #113
			     '[88] TR-808 Snare1', #114
			     '[88] TR-808 Snare2', #115
			     'TR-808 Snare2', #116
			     '[88] TR-909 Snare1', #117
			     '[88] TR-909 Snare2', #118
			     'TR-909 Snare1', #119
			     'TR-909 Snare2', #120
			     'Rap Snare', #121
			     'Jungle Snare1', #122
			     'House Snare1', #123
			     '[88] House Snare', #124
			     'House Snare2', #125
			     'Voice Tah', #126
			     '[88] Slappy', #127
			   ],
	       },
	       {
		   :name => 'JAZZ',
		   :prog => 33,
		   :list => [
			     '[88] Standard1 Kick1', #0
			     '[88] Standard1 Kick2', #1
			     '[88] Standard2 Kick1', #2
			     '[88] Standard2 Kick2', #3
			     '[55] Kick Drum1', #4
			     '[55] Kick Drum2', #5
			     '[88] Jazz Kick1', #6
			     '[88] Jazz Kick2', #7
			     '[88] Room Kick1', #8
			     '[88] Room Kick2', #9
			     '[88] Power Kick1', #10
			     '[88] Power Kick2', #11
			     '[88] Electric Kick2', #12
			     '[88] Electric Kick1', #13
			     '[88] TR-808 Kick', #14
			     '[88] TR-909 Kick', #15
			     '[88] Dance Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     'MC-505 Beep1', #24
			     'MC-505 Beep2', #25
			     'Concert SD', #26
			     'Snare Roll', #27
			     'Finger Snap', #28
			     'High Q', #29
			     'Slap', #30
			     'Scratch Push', #31
			     'Scratch Pull', #32
			     'Sticks', #33
			     'Square Click', #34
			     'Metronome Click', #35
			     'Metronome Bell', #36
			     'Jazz BD 2', #37
			     'Jazz BD 1', #38
			     'Side Stick', #39
			     'Snare Drum 1', #40
			     'Hand Clap', #41
			     'Snare Drum 2', #42
			     'Low Tom 2', #43
			     'Closed Hi-Hat', #44
			     'Low Tom 1', #45
			     'Pedal Hi-Hat', #46
			     'Mid Tom 2', #47
			     'Open Hi-Hat', #48
			     'Mid Tom 1', #49
			     'High Tom 2', #50
			     'Crash Cymbal 1', #51
			     'High Tom 1', #52
			     'Ride Cymbal 1', #53
			     'Chinese Cymbal', #54
			     'Ride Bell', #55
			     'Tambourine', #56
			     'Splash Cymbal', #57
			     'Cowbell', #58
			     'Crash Cymbal 2', #59
			     'Vibra-slap', #60
			     'Ride Cymbal 2', #61
			     'High Bongo', #62
			     'Low Bongo', #63
			     'Mute High Conga', #64
			     'Open High Conga', #65
			     'Low Conga', #66
			     'High Timbale', #67
			     'Low Timbale', #68
			     'High Agogo', #69
			     'Low Agogo', #70
			     'Cabasa', #71
			     'Maracas', #72
			     'Short Hi Whistle', #73
			     'Long Low Whistle', #74
			     'Short Guiro', #75
			     'Long Guiro', #76
			     'Claves', #77
			     'High Wood Block', #78
			     'Low Wood Block', #79
			     'Mute Cuica', #80
			     'Open Cuica', #81
			     'Mute Triangle', #82
			     'Open Triangle', #83
			     'Shaker', #84
			     'Jingle Bell', #85
			     'Belltree', #86
			     'Castanets', #87
			     'Mute Surdo', #88
			     'Open Surdo', #89
			     'Applause', #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     '[88] Standard1 Snare1', #97
			     '[88] Standard1 Snare2', #98
			     '[88] Standard2 Snare1', #99
			     '[88] Brush Tap1', #100
			     '[88] Brush Tap2', #101
			     '[88] Brush Slap1', #102
			     '[88] Brush Slap2', #103
			     '[88] Brush Slap3', #104
			     '[88] Brush Swirl1', #105
			     '[88] Brush Swirl2', #106
			     '[88] Brush Long Swirl', #107
			     '[88] Jazz Snare1', #108
			     '[88] Jazz Snare2', #109
			     '[88] Standard 1 Snare 1', #110
			     '[88] Standard 1 Snare 2', #111
			     '[88] Standard 2 Snare 1', #112
			     '[88] Standard 2 Snare 2', #113
			     '[55] Snare Drum2', #114
			     'Standard1 Snare1', #115
			     'Standard1 Snare2', #116
			     'Standard Snare3', #117
			     '[88] Room Snare1', #118
			     '[88] Room Snare2', #119
			     '[88] Power Snare1', #120
			     '[88] Power Snare2', #121
			     '[88] Gated Snare', #122
			     '[88] Dance Snare1', #123
			     '[88] Dance Snare2', #124
			     '[88] Disco Snare', #125
			     '[88] Electric Snare2', #126
			     '[88] Electric Snare3', #127
			   ],
	       },
	       {
		   :name => 'BRUSH',
		   :prog => 41,
		   :list => [
			     '[88] Standard1 Kick1', #0
			     '[88] Standard1 Kick2', #1
			     '[88] Standard2 Kick1', #2
			     '[88] Standard2 Kick2', #3
			     '[55] Kick Drum1', #4
			     '[55] Kick Drum2', #5
			     '[88] Jazz Kick1', #6
			     '[88] Jazz Kick2', #7
			     '[88] Room Kick1', #8
			     '[88] Room Kick2', #9
			     '[88] Power Kick1', #10
			     '[88] Power Kick2', #11
			     '[88] Electric Kick2', #12
			     '[88] Electric Kick1', #13
			     '[88] TR-808 Kick', #14
			     '[88] TR-909 Kick', #15
			     '[88] Dance Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     'MC-505 Beep1', #24
			     'MC-505 Beep2', #25
			     'Concert SD', #26
			     'Snare Roll', #27
			     'Finger Snap', #28
			     'High Q', #29
			     'Slap', #30
			     'Scratch Push', #31
			     'Scratch Pull', #32
			     'Sticks', #33
			     'Square Click', #34
			     'Metronome Click', #35
			     'Metronome Bell', #36
			     'Jazz BD 2', #37
			     'Jazz BD 1', #38
			     'Side Stick', #39
			     'Brush Tap', #40
			     'Brush Slap', #41
			     'Brush Swirl', #42
			     'Low Tom 2', #43
			     'Closed Hi-Hat', #44
			     'Low Tom 1', #45
			     'Pedal Hi-Hat', #46
			     'Mid Tom 2', #47
			     'Open Hi-Hat', #48
			     'Mid Tom 1', #49
			     'High Tom 2', #50
			     'Crash Cymbal 1', #51
			     'High Tom 1', #52
			     'Ride Cymbal 1', #53
			     'Chinese Cymbal', #54
			     'Ride Bell', #55
			     'Tambourine', #56
			     'Splash Cymbal', #57
			     'Cowbell', #58
			     'Crash Cymbal 2', #59
			     'Vibra-slap', #60
			     'Ride Cymbal 2', #61
			     'High Bongo', #62
			     'Low Bongo', #63
			     'Mute High Conga', #64
			     'Open High Conga', #65
			     'Low Conga', #66
			     'High Timbale', #67
			     'Low Timbale', #68
			     'High Agogo', #69
			     'Low Agogo', #70
			     'Cabasa', #71
			     'Maracas', #72
			     'Short Hi Whistle', #73
			     'Long Low Whistle', #74
			     'Short Guiro', #75
			     'Long Guiro', #76
			     'Claves', #77
			     'High Wood Block', #78
			     'Low Wood Block', #79
			     'Mute Cuica', #80
			     'Open Cuica', #81
			     'Mute Triangle', #82
			     'Open Triangle', #83
			     'Shaker', #84
			     'Jingle Bell', #85
			     'Belltree', #86
			     'Castanets', #87
			     'Mute Surdo', #88
			     'Open Surdo', #89
			     'Applause', #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     '[88] Standard1 Snare1', #97
			     '[88] Standard1 Snare2', #98
			     '[88] Standard2 Snare1', #99
			     '[88] Standard2 Snare2', #100
			     '[55] Snare Drum2', #101
			     'Standard1 Snare1', #102
			     'Standard1 Snare2', #103
			     'Standard Snare3', #104
			     '[88] Jazz Snare1', #105
			     '[88] Jazz Snare2', #106
			     '[88] Room Snare1', #107
			     '[88] Room Snare2', #108
			     '[88] Power Snare1', #109
			     '[88] Power Snare2', #110
			     '[55] Gated Snare', #111
			     '[88] Dance Snare1', #112
			     '[88] Dance Snare2', #113
			     '[88] Disco Snare', #114
			     '[88] Electric Snare2', #115
			     '[55] Electric Snare', #116
			     '[88] Electric Snare 3', #117
			     'TR-707 Snare', #118
			     '[88] TR-808 Snare1', #119
			     '[88] TR-808 Snare2', #120
			     '[88] TR-909 Snare1', #121
			     '[88] TR-909 Snare2', #122
			     'Rap Snare', #123
			     'Jungle Snare1', #124
			     'House Snare1', #125
			     '[88] House Snare', #126
			     'House Snare2', #127
			   ],
	       },
	       {
		   :name => 'ORCHESTRA',
		   :prog => 49,
		   :list => [
			     '[88] Standard1 Kick1', #0
			     '[88] Standard1 Kick2', #1
			     '[88] Standard2 Kick1', #2
			     '[88] Standard2 Kick2', #3
			     '[55] Kick Drum1', #4
			     '[55] Kick Drum2', #5
			     '[88] Jazz Kick1', #6
			     '[88] Jazz Kick2', #7
			     '[88] Room Kick1', #8
			     '[88] Room Kick2', #9
			     '[88] Power Kick1', #10
			     '[88] Power Kick2', #11
			     '[88] Electric Kick2', #12
			     '[88] Electric Kick1', #13
			     '[88] TR-808 Kick', #14
			     '[88] TR-909 Kick', #15
			     '[88] Dance Kick', #16
			     'Voice One', #17
			     'Voice Two', #18
			     'Voice Three', #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     'MC-505 Beep1', #24
			     'MC-505 Beep2', #25
			     'Concert SD', #26
			     'Snare Roll', #27
			     'Finger Snap', #28
			     'Closed Hi-Hat', #29
			     'Pedal Hi-Hat', #30
			     'Open Hi-Hat', #31
			     'Ride Cymbal', #32
			     'Sticks', #33
			     'Square Click', #34
			     'Metronome Click', #35
			     'Metronome Bell', #36
			     'Concert BD2', #37
			     'Concert BD1', #38
			     'Side Stick', #39
			     'Concert SD', #40
			     'Castanets', #41
			     'Concert SD', #42
			     'Timpani F', #43
			     'Timpani F#', #44
			     'Timpani G', #45
			     'Timpani G#', #46
			     'Timpani A', #47
			     'Timpani A#', #48
			     'Timpani B', #49
			     'Timpani c', #50
			     'Timpani c#', #51
			     'Timpani d', #52
			     'Timpani d#', #53
			     'Timpani e', #54
			     'Timpani f', #55
			     'Tambourine', #56
			     'Splash Cymbal', #57
			     'Cowbell', #58
			     'Concert Cymbal2', #59
			     'Vibra-slap', #60
			     'Concert Cymbal1', #61
			     'High Bongo', #62
			     'Low Bongo', #63
			     'Mute High Conga', #64
			     'Open High Conga', #65
			     'Open Low Conga', #66
			     'High Timbale', #67
			     'Low Timbale', #68
			     'High Agogo', #69
			     'Low Agogo', #70
			     'Cabasa', #71
			     'Maracas', #72
			     'Short High Whisle', #73
			     'Long Low Whisle', #74
			     'Short Guiro', #75
			     'Long Guiro', #76
			     'Claves', #77
			     'High Wood Block', #78
			     'Low Wood Block', #79
			     'Mute Cuica', #80
			     'Open Cuica', #81
			     'Mute Triangle', #82
			     'Open Triangle', #83
			     'Shaker', #84
			     'Jingle Bell', #85
			     'Bell Tree', #86
			     'Castanets', #87
			     'Mute Surdo', #88
			     'Open Surdo', #89
			     'Applause', #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     'Applaus2', #97
			     'Small Club1', #98
			     '[55] Timpani D#', #99
			     '[55] Timpani E', #100
			     '[55] Timpani F', #101
			     '[55] Timpani F#', #102
			     '[55] Timpani G', #103
			     '[55] Timpani G#', #104
			     '[55] Timpani A', #105
			     '[55] Timpani A#', #106
			     '[55] Timpani B', #107
			     '[55] Timpani c', #108
			     '[55] Timpani c#', #109
			     '[55] Timpani d', #110
			     '[55] Timpani d#', #111
			     '[55] Timpani e', #112
			     '[55] Timpani f', #113
			     nil, #114
			     nil, #115
			     nil, #116
			     nil, #117
			     nil, #118
			     nil, #119
			     nil, #120
			     nil, #121
			     nil, #122
			     nil, #123
			     nil, #124
			     nil, #125
			     nil, #126
			     nil, #127
			   ],
	       },
	       {
		   :name => 'ETHNIC',
		   :prog => 50,
		   :list => [
			     nil, #0
			     nil, #1
			     nil, #2
			     nil, #3
			     nil, #4
			     nil, #5
			     nil, #6
			     nil, #7
			     nil, #8
			     nil, #9
			     nil, #10
			     nil, #11
			     nil, #12
			     nil, #13
			     nil, #14
			     nil, #15
			     nil, #16
			     nil, #17
			     nil, #18
			     nil, #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     nil, #24
			     'Finger Snap', #25
			     'Tambourine', #26
			     'Castanets', #27
			     'Crash Cymbal1', #28
			     'Snare Roll', #29
			     'Concert SD', #30
			     'Concert Cymbal', #31
			     'Concert BD1', #32
			     'Jingle Bell', #33
			     'Bell Tree', #34
			     'Bar Chimes', #35
			     'Wadaiko', #36
			     'Wadaiko Rim', #37
			     'Shime Taiko', #38
			     'Atarigane', #39
			     'Hyoushigi', #40
			     'Ohkawa', #41
			     'High Kotsuzumi', #42
			     'Low Kotsuzumi', #43
			     'Ban Gu', #44
			     'Big Gong', #45
			     'Small Gong', #46
			     'Bend Gong', #47
			     'Thai Gong', #48
			     'Rama Cymbal', #49
			     'Gamelan Gong', #50
			     'Udo Short', #51
			     'Udo Long', #52
			     'Udo Slap', #53
			     'Bendir', #54
			     'Req Dum', #55
			     'Req Tik', #56
			     'Tabla Te', #57
			     'Tabla Na', #58
			     'Tabla Tun', #59
			     'Tabla Ge', #60
			     'Tabla Ge Hi', #61
			     'Talking Drum', #62
			     'Bend Talking Drum', #63
			     'Caxixi', #64
			     'Djembe', #65
			     'Djembe Rim', #66
			     'Timbales Low', #67
			     'Timbales Paila', #68
			     'Timbales High', #69
			     'Cowbell', #70
			     'High Bongo', #71
			     'Low Bongo', #72
			     'Mute High Conga', #73
			     'Open High Conga', #74
			     'Mute Low Conga', #75
			     'Conga Slap', #76
			     'Open Low Conga', #77
			     'Conga Slide', #78
			     'Mute Pandiero', #79
			     'Open Pandiero', #80
			     'Open Surdo', #81
			     'Mute Surdo', #82
			     'Tamborim', #83
			     'High Agogo', #84
			     'Low Agogo', #85
			     'Shaker', #86
			     'High Whistle', #87
			     'Low Whistle', #88
			     'Mute Cuica', #89
			     'Open Cuica', #90
			     'Mute Triangle', #91
			     'Open Triangle', #92
			     'Short Guiro', #93
			     'Long Guiro', #94
			     'Cabasa Up', #95
			     'Cabasa Down', #96
			     'Claves', #97
			     'High Wood Block', #98
			     'Low Wood Block', #99
			     nil, #100
			     nil, #101
			     nil, #102
			     nil, #103
			     nil, #104
			     nil, #105
			     nil, #106
			     nil, #107
			     nil, #108
			     nil, #109
			     nil, #110
			     nil, #111
			     nil, #112
			     nil, #113
			     nil, #114
			     nil, #115
			     nil, #116
			     nil, #117
			     nil, #118
			     nil, #119
			     nil, #120
			     nil, #121
			     nil, #122
			     nil, #123
			     nil, #124
			     nil, #125
			     nil, #126
			     nil, #127
			   ],
	       },
	       {
		   :name => 'KICK & SNARE',
		   :prog => 51,
		   :list => [
			     nil, #0
			     nil, #1
			     nil, #2
			     nil, #3
			     nil, #4
			     nil, #5
			     nil, #6
			     nil, #7
			     nil, #8
			     nil, #9
			     nil, #10
			     nil, #11
			     nil, #12
			     nil, #13
			     nil, #14
			     nil, #15
			     nil, #16
			     nil, #17
			     nil, #18
			     nil, #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     nil, #24
			     'CR-78 Kick1', #25
			     'CR-78 Kick2', #26
			     'TR-606 Kick1', #27
			     'TR-707 Kick', #28
			     'TR-808 Kick', #29
			     'Hip-Hop Kick2', #30
			     'TR-909 Kick1', #31
			     'Hip-Hop Kick3', #32
			     'Hip-Hop Kick1', #33
			     'Jungle Kick2', #34
			     'Jungle Kick1', #35
			     'Techno Kick2', #36
			     'Techno Kick1', #37
			     'Standard1 Kick2', #38
			     'Standard1 Kick1', #39
			     '[88] Standard1 Kick1', #40
			     '[88] Standard1 Kick2', #41
			     '[88] Standard2 Kick1', #42
			     '[88] Standard2 Kick2', #43
			     '[55] Kick Drum1', #44
			     '[55] Kick Drum2', #45
			     '[88] Soft Kick', #46
			     '[88] Jazz Kick1', #47
			     '[88] Jazz Kick2', #48
			     '[55] Concert BD1', #49
			     '[88] Room Kick1', #50
			     '[88] Room Kick2', #51
			     '[88] Power Kick1', #52
			     '[88] Power Kick2', #53
			     '[88] Electric Kick2', #54
			     '[88] Electric Kick1', #55
			     '[55] Electric Kick', #56
			     '[88] TR-808 Kick', #57
			     '[88] TR-909 Kick', #58
			     '[88] Dance Kick', #59
			     '[88] Standard1 Snare1', #60
			     '[88] Standard1 Snare2', #61
			     '[88] Standard2 Snare1', #62
			     '[88] Standard2 Snare2', #63
			     '[55] Snare Drum2', #64
			     '[55] Concert Snare', #65
			     '[88] Jazz Snare1', #66
			     '[88] Jazz Snare2', #67
			     '[88] Room Snare1', #68
			     '[88] Room Snare2', #69
			     '[88] Power Snare1', #70
			     '[88] Power Snare2', #71
			     '[55] Gated Snare', #72
			     '[88] Dance Snare1', #73
			     '[88] Dance Snare2', #74
			     '[88] Disco Snare', #75
			     '[88] Electric Snare2', #76
			     '[88] House Snare', #77
			     '[55] Electric Snare1', #78
			     '[88] Electric Snare3', #79
			     '[88] TR-808 Snare1', #80
			     '[88] TR-808 Snare2', #81
			     '[88] TR-909 Snare1', #82
			     '[88] TR-909 Snare2', #83
			     '[88] Brush Tap1', #84
			     '[88] Brush Tap2', #85
			     '[88] Brush Slap1', #86
			     '[88] Brush Slap2', #87
			     '[88] Brush Slap3', #88
			     '[88] Brush Swirl1', #89
			     '[88] Brush Swirl2', #90
			     '[88] Brush Long Swirl', #91
			     'Standard1 Snare1', #92
			     'Standard1 Snare2', #93
			     'Standard Snare3', #94
			     'Rap Snare', #95
			     'Hip-Hop Snare2', #96
			     'Jungle Snare1', #97
			     'Jungle Snare2', #98
			     'Techno Snare1', #99
			     'Techno Snare2', #100
			     'House Snare2', #101
			     'CR-78 Snare1', #102
			     'CR-78 Snare2', #103
			     'TR-606 Snare1', #104
			     'TR-606 Snare2', #105
			     'TR-707 Snare1', #106
			     'TR-707 Snare2', #107
			     'Standard3 Snare2', #108
			     'TR-808 Snare2', #109
			     'TR-909 Snare1', #110
			     'TR-909 Snare2', #111
			     nil, #112
			     nil, #113
			     nil, #114
			     nil, #115
			     nil, #116
			     nil, #117
			     nil, #118
			     nil, #119
			     nil, #120
			     nil, #121
			     nil, #122
			     nil, #123
			     nil, #124
			     nil, #125
			     nil, #126
			     nil, #127
			   ],
	       },
	       {
		   :name => 'ASIA',
		   :prog => 53,
		   :list => [
			     nil, #0
			     nil, #1
			     nil, #2
			     nil, #3
			     nil, #4
			     nil, #5
			     nil, #6
			     nil, #7
			     nil, #8
			     nil, #9
			     nil, #10
			     nil, #11
			     nil, #12
			     nil, #13
			     nil, #14
			     nil, #15
			     nil, #16
			     nil, #17
			     nil, #18
			     nil, #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     nil, #24
			     'Gamelan Gong C#', #25
			     'Gamelan Gong D#', #26
			     'Gamelan Gong G', #27
			     'Gamelan Gong A#', #28
			     'Gamelan Gong c', #29
			     'Gamelan Gong c#', #30
			     'Gamelan Gong d#', #31
			     'Gamelan Gong g', #32
			     'Gamelan Gong a#', #33
			     'Gamelan Gong C', #34
			     'Gender C#', #35
			     'Gender D#', #36
			     'Gender G', #37
			     'Gender A#', #38
			     'Gender c', #39
			     'Bonang C#', #40
			     'Bonang D#', #41
			     'Bonang G', #42
			     'Bonang A#', #43
			     'Bonang c', #44
			     'Thai Gong', #45
			     'Rama Cymbal', #46
			     'Sagat Open', #47
			     'Sagat Closed', #48
			     'Jaws Harp', #49
			     'Wadaiko', #50
			     'Wadaiko Rim', #51
			     'Taiko', #52
			     'Shimedaiko', #53
			     'Atarigane', #54
			     'Hyousigi', #55
			     'Ohkawa', #56
			     'High Kotsuzumi', #57
			     'Low Kotsuzumi', #58
			     'Yyoo Dude', #59
			     'Buk', #60
			     'Buk Rim', #61
			     'Gengari p', #62
			     'Gengari Mute Low', #63
			     'Gengari f', #64
			     'Gengari Mute High', #65
			     'Gengari Samll', #66
			     'Jang-Gu Che', #67
			     'Jang-Gu Kun', #68
			     'Jang-Gu Rim', #69
			     'Jing p', #70
			     'Jing f', #71
			     'Jing Mute', #72
			     'Asian Gong1', #73
			     'Big Gong', #74
			     'Small Gong', #75
			     'Pai Ban', #76
			     'Ban Gu', #77
			     'Tang Gu', #78
			     'Tnag Gu Mute', #79
			     'Shou Luo', #80
			     'Bend Gong', #81
			     'Hu Yin Luo Low', #82
			     'Hu Yin Luo Mid', #83
			     'Hu Yin Luo Mid Mute', #84
			     'Hu Yin Luo High', #85
			     'Hu Yin Luo High Mute', #86
			     'Nao Bo', #87
			     'Xiao Bo', #88
			     nil, #89
			     nil, #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     nil, #97
			     nil, #98
			     nil, #99
			     nil, #100
			     nil, #101
			     nil, #102
			     nil, #103
			     nil, #104
			     nil, #105
			     nil, #106
			     nil, #107
			     nil, #108
			     nil, #109
			     nil, #110
			     nil, #111
			     nil, #112
			     nil, #113
			     nil, #114
			     nil, #115
			     nil, #116
			     nil, #117
			     nil, #118
			     nil, #119
			     nil, #120
			     nil, #121
			     nil, #122
			     nil, #123
			     nil, #124
			     nil, #125
			     nil, #126
			     nil, #127
			   ],
	       },
	       {
		   :name => 'CYMBAL & CLAPS',
		   :prog => 54,
		   :list => [
			     nil, #0
			     nil, #1
			     nil, #2
			     nil, #3
			     nil, #4
			     nil, #5
			     nil, #6
			     nil, #7
			     nil, #8
			     nil, #9
			     nil, #10
			     nil, #11
			     nil, #12
			     nil, #13
			     nil, #14
			     nil, #15
			     nil, #16
			     nil, #17
			     nil, #18
			     nil, #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     nil, #24
			     nil, #25
			     nil, #26
			     nil, #27
			     nil, #28
			     nil, #29
			     nil, #30
			     nil, #31
			     'Reverse Open Hi-Hat', #32
			     'Reverse Closed Hi-Hat1', #33
			     'Reverse Closed Hi-Hat2', #34
			     'Jungle Hi-Hat', #35
			     '[55] Closed Hi-hat', #36
			     '[88] Closed Hi-hat2', #37
			     '[88] Closed Hi-hat3', #38
			     'Closed Hi-Hat4', #39
			     'Closed Hi-Hat', #40
			     'TR-707 Closed Hi-Hat', #41
			     'TR-606 Closed Hi-Hat', #42
			     '[88] TR-808 Closed Hi-Hat', #43
			     'TR-808 Closed Hi-Hat', #44
			     'CR-78 Closed Hi-Hat', #45
			     '[55] Pedal Hi-Hat', #46
			     '[88] Pedal Hi-Hat', #47
			     'Pedal Hi-Hat', #48
			     'Half-Open Hi-Hat1', #49
			     'Half-Open Hi-Hat2', #50
			     '[55] Open Hi-Hat', #51
			     '[88] Open Hi-Hat2', #52
			     '[88] Open Hi-Hat3', #53
			     'Open Hi-Hat', #54
			     'TR-909 Open Hi-Hat', #55
			     'TR-707 Open Hi-Hat', #56
			     'TR-606 Open Hi-Hat', #57
			     '[88] TR-808 Open Hi-Hat', #58
			     'TR-808 Open Hi-Hat', #59
			     'CR-78 Open Hi-Hat', #60
			     'Crash Cymbal1', #61
			     'Crash Cymbal2', #62
			     'Crash Cymbal3', #63
			     'Brush Crash Cymbal', #64
			     'Hard Crash Cymbal', #65
			     'TR-909 Crash Cymbal', #66
			     'TR-808 Crash Cymbal', #67
			     'Mute Crash Cymbal1', #68
			     'Mute Crash Cymbal2', #69
			     'Reverse Crash Cymbal1', #70
			     'Reverse Crash Cymbal2', #71
			     'Reverse Crash Cymbal3', #72
			     'Reverse TR-909 Crash Cymbal', #73
			     '[55] Splash Cymbal', #74
			     'Splash Cymbal', #75
			     '[88] Ride Bell', #76
			     '[88] Brush Ride Bell', #77
			     '[88] Ride Cymbal1', #78
			     '[88] Ride Cymbal2', #79
			     '[88] Brush Ride Cymbal', #80
			     'Ride Cymbal Low Inner', #81
			     'Ride Cymbal Mid Inner', #82
			     'Ride Cymbal High Inner', #83
			     'Ride Cymbal Low Edge', #84
			     'Ride Cymbal Mid Edge', #85
			     'Ride Cymbal High Edge', #86
			     'TR-606 Ride Cymbal', #87
			     'TR-808 Ride Cymbal', #88
			     'Chinese Cymbal1', #89
			     'Chinese Cymbal2', #90
			     '[55] Hand Clap', #91
			     '[88] Hand Clap2', #92
			     '[88] Hand Clap', #93
			     'Hand Clap', #94
			     'Hand Clap2', #95
			     'TR-707 Hand Clap', #96
			     nil, #97
			     nil, #98
			     nil, #99
			     nil, #100
			     nil, #101
			     nil, #102
			     nil, #103
			     nil, #104
			     nil, #105
			     nil, #106
			     nil, #107
			     nil, #108
			     nil, #109
			     nil, #110
			     nil, #111
			     nil, #112
			     nil, #113
			     nil, #114
			     nil, #115
			     nil, #116
			     nil, #117
			     nil, #118
			     nil, #119
			     nil, #120
			     nil, #121
			     nil, #122
			     nil, #123
			     nil, #124
			     nil, #125
			     nil, #126
			     nil, #127
			   ],
	       },
	       {
		   :name => 'SFX',
		   :prog => 57,
		   :list => [
			     nil, #0
			     nil, #1
			     nil, #2
			     nil, #3
			     nil, #4
			     nil, #5
			     nil, #6
			     nil, #7
			     nil, #8
			     nil, #9
			     nil, #10
			     nil, #11
			     nil, #12
			     nil, #13
			     nil, #14
			     nil, #15
			     nil, #16
			     nil, #17
			     nil, #18
			     nil, #19
			     nil, #20
			     'MC-505 Beep1', #21
			     'MC-505 Beep2', #22
			     'Guitar Slide', #23
			     'Guitar Wah', #24
			     'Guitar Slap', #25
			     'Chord Stroke Down', #26
			     'Chord Stroke Up', #27
			     'Biwa', #28
			     'Phonograph Noise', #29
			     'Tape Rewind', #30
			     'Scratch Push2', #31
			     'Scratch Pull2', #32
			     'Cutting Noise 2 Up', #33
			     'Cutting Noise 2 Down', #34
			     'Distortion Guitar Cutting Noise Up', #35
			     'Distortion Guitar Cutting Noise Down', #36
			     'Bass Slide', #37
			     'Pick Scrape', #38
			     'High Q', #39
			     'Slap', #40
			     'Scratch Push', #41
			     'Scratch Pull', #42
			     'Sticks', #43
			     'Square Click', #44
			     'Metronome Click', #45
			     'Metronome Bell', #46
			     'Guitar Fret Noise', #47
			     'Guitar Cutting Noise Up', #48
			     'Guitar Cutting Noise Down', #49
			     'String Slap of Double Bass', #50
			     'Flute Key Click Noise', #51
			     'Laughing', #52
			     'Screaming', #53
			     'Punch', #54
			     'Heart Beat', #55
			     'Footsteps1', #56
			     'Footsteps2', #57
			     'Applause', #58
			     'Door Creaking', #59
			     'Door', #60
			     'Scratch', #61
			     'Wind Chimes', #62
			     'Car Engine', #63
			     'Car Stop', #64
			     'Car Passing', #65
			     'Car Crash', #66
			     'Siren', #67
			     'Train', #68
			     'Jetplane', #69
			     'Helicopter', #70
			     'Starship', #71
			     'Gun Shot', #72
			     'Machine Gun', #73
			     'Laser Gun', #74
			     'Explosion', #75
			     'Dog', #76
			     'Horse-Gallop', #77
			     'Birds', #78
			     'Rain', #79
			     'Thunder', #80
			     'Wind', #81
			     'Seashore', #82
			     'Stream', #83
			     'Bubble', #84
			     'Kitty', #85
			     'Bird2', #86
			     'Growl', #87
			     'Applause2', #88
			     'Telephone1', #89
			     'Telephone2', #90
			     'Small Club1', #91
			     'Small Club2', #92
			     'Applause Wave', #93
			     'Eruption', #94
			     'Big Shot', #95
			     'Percussion Bang', #96
			     nil, #97
			     nil, #98
			     nil, #99
			     nil, #100
			     nil, #101
			     nil, #102
			     nil, #103
			     nil, #104
			     nil, #105
			     nil, #106
			     nil, #107
			     nil, #108
			     nil, #109
			     nil, #110
			     nil, #111
			     nil, #112
			     nil, #113
			     nil, #114
			     nil, #115
			     nil, #116
			     nil, #117
			     nil, #118
			     nil, #119
			     nil, #120
			     nil, #121
			     nil, #122
			     nil, #123
			     nil, #124
			     nil, #125
			     nil, #126
			     nil, #127
			   ],
	       },
	       {
		   :name => 'RHYTHM FX',
		   :prog => 58,
		   :list => [
			     nil, #0
			     nil, #1
			     nil, #2
			     nil, #3
			     nil, #4
			     nil, #5
			     nil, #6
			     nil, #7
			     nil, #8
			     nil, #9
			     nil, #10
			     nil, #11
			     nil, #12
			     nil, #13
			     nil, #14
			     nil, #15
			     nil, #16
			     nil, #17
			     nil, #18
			     nil, #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     nil, #24
			     nil, #25
			     nil, #26
			     nil, #27
			     nil, #28
			     nil, #29
			     nil, #30
			     nil, #31
			     nil, #32
			     nil, #33
			     nil, #34
			     nil, #35
			     'Reverse Kick1', #36
			     'Reverse Concert Bass Drum', #37
			     'Reverse Power Kick1', #38
			     'Reverse Electric Kick1', #39
			     'Reverse Snare1', #40
			     'Reverse Snare2', #41
			     'Reverse Standard1 Snare1', #42
			     'Reverse Snare Drum2', #43
			     'Reverse Tight Snare', #44
			     'Reverse 808 Snare', #45
			     'Reverse Tom1', #46
			     'Reverse Tom2', #47
			     'Reverse Sticks', #48
			     'Reverse Slap', #49
			     'Reverse Cymbal1', #50
			     'Reverse Cymbal2', #51
			     'Reverse Open Hi-hat', #52
			     'Reverse Ride Cymbal', #53
			     'Reverse CR-78 Open Hi-Hat', #54
			     'Reverse Closed Hi-hat', #55
			     'Reverse Gong', #56
			     'Reverse Bell Tree', #57
			     'Reverse Guiro', #58
			     'Reverse Bendir', #59
			     'Reverse Gun Shot', #60
			     'Reverse Scratch', #61
			     'Reverse Laser Gun', #62
			     'Key Click', #63
			     'Techno Trip', #64
			     'Pop Drop', #65
			     'Woody Slap', #66
			     'Distortion Kick', #67
			     'Syn.Drop', #68
			     'Reverse High Q', #69
			     'Pipe', #70
			     'Ice Block', #71
			     'Digital Tambourine', #72
			     'Alias', #73
			     'Modulated Bell', #74
			     'Spark', #75
			     'Metalic Percussion', #76
			     'Velocity Noise FX', #77
			     'Stereo Noise Clap', #78
			     'Swish', #79
			     'Slappy', #80
			     'Voice Ou', #81
			     'Voice Au', #82
			     'Hoo', #83
			     'Tape Stop1', #84
			     'Tape Stop2', #85
			     'Missile', #86
			     'Space Bird', #87
			     'Flying Monster', #88
			     nil, #89
			     nil, #90
			     nil, #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     nil, #97
			     nil, #98
			     nil, #99
			     nil, #100
			     nil, #101
			     nil, #102
			     nil, #103
			     nil, #104
			     nil, #105
			     nil, #106
			     nil, #107
			     nil, #108
			     nil, #109
			     nil, #110
			     nil, #111
			     nil, #112
			     nil, #113
			     nil, #114
			     nil, #115
			     nil, #116
			     nil, #117
			     nil, #118
			     nil, #119
			     nil, #120
			     nil, #121
			     nil, #122
			     nil, #123
			     nil, #124
			     nil, #125
			     nil, #126
			     nil, #127
			   ],
	       },
	       {
		   :name => 'RHYTHM FX 2',
		   :prog => 59,
		   :list => [
			     nil, #0
			     nil, #1
			     nil, #2
			     nil, #3
			     nil, #4
			     nil, #5
			     nil, #6
			     nil, #7
			     nil, #8
			     nil, #9
			     nil, #10
			     nil, #11
			     nil, #12
			     nil, #13
			     nil, #14
			     nil, #15
			     nil, #16
			     nil, #17
			     nil, #18
			     nil, #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     nil, #24
			     nil, #25
			     nil, #26
			     nil, #27
			     nil, #28
			     nil, #29
			     nil, #30
			     nil, #31
			     nil, #32
			     nil, #33
			     nil, #34
			     nil, #35
			     'Reverse TR-707 Kick1', #36
			     'Reverse TR-909 Kick1', #37
			     'Reverse Hip-Hop Kick1', #38
			     'Reverse Jungle Kick2', #39
			     'Reverse Techno Kick2', #40
			     'Reverse TR-606 Snare2', #41
			     'Reverse CR-78 Snare1', #42
			     'Reverse CR-78 Snare2', #43
			     'Reverse Jungle Snare2', #44
			     'Reverse Techno Snare2', #45
			     'Reverse TR-707 Snare', #46
			     'Reverse TR-606 Snare1', #47
			     'Reverse TR-909 Snare1', #48
			     'Reverse Hip-Hop Snare2', #49
			     'Reverse Jungle Snare1', #50
			     'Reverse House Snare', #51
			     'Reverse Closed Hi-Hat', #52
			     'Reverse TR-606 Closed Hi-Hat', #53
			     'Reverse TR-707 Closed Hi-Hat', #54
			     'Reverse TR-808 Closed Hi-Hat', #55
			     'Reverse Jungle Hi-Hat', #56
			     'Reverse Tambourine2', #57
			     'Reverse Shake Tambourine', #58
			     'Reverse TR-808 Open Hi-Hat', #59
			     'Reverse TR-707 Open Hi-Hat', #60
			     'Reverse Open Hi-Hat', #61
			     'Reverse TR-606 Open Hi-Hat', #62
			     'Reverse Hu Yin Luo', #63
			     'Reverse TR-707 Crash Cymbal', #64
			     'Voice One', #65
			     'Reverse Voice One', #66
			     'Voice Two', #67
			     'Reverse Voice Two', #68
			     'Voice Three', #69
			     'Reverse Voice Three', #70
			     'Voice Tah', #71
			     'Reverse Voice Tah', #72
			     'Voice Ou', #73
			     'Voice Au', #74
			     'Voice Whey', #75
			     'Frog Vpoce', #76
			     'Reverse Yyoo Dude', #77
			     'Douby', #78
			     'Reverse Douby', #79
			     'Baert High', #80
			     'Baert Low', #81
			     'Bounce', #82
			     'Reverse Bounce', #83
			     'Dstortion Knock', #84
			     'Guitar Slide', #85
			     'Sub Marine', #86
			     'Noise Attack', #87
			     'Space Worms', #88
			     'Emergency!', #89
			     'Calculating...', #90
			     'Saw LFO Saw', #91
			     nil, #92
			     nil, #93
			     nil, #94
			     nil, #95
			     nil, #96
			     nil, #97
			     nil, #98
			     nil, #99
			     nil, #100
			     nil, #101
			     nil, #102
			     nil, #103
			     nil, #104
			     nil, #105
			     nil, #106
			     nil, #107
			     nil, #108
			     nil, #109
			     nil, #110
			     nil, #111
			     nil, #112
			     nil, #113
			     nil, #114
			     nil, #115
			     nil, #116
			     nil, #117
			     nil, #118
			     nil, #119
			     nil, #120
			     nil, #121
			     nil, #122
			     nil, #123
			     nil, #124
			     nil, #125
			     nil, #126
			     nil, #127
			   ],
	       },
	       {
		   :name => 'CM-64/32L Compatible',
		   :prog => 128,
		   :list => [
			     nil, #0
			     nil, #1
			     nil, #2
			     nil, #3
			     nil, #4
			     nil, #5
			     nil, #6
			     nil, #7
			     nil, #8
			     nil, #9
			     nil, #10
			     nil, #11
			     nil, #12
			     nil, #13
			     nil, #14
			     nil, #15
			     nil, #16
			     nil, #17
			     nil, #18
			     nil, #19
			     nil, #20
			     nil, #21
			     nil, #22
			     nil, #23
			     nil, #24
			     nil, #25
			     nil, #26
			     nil, #27
			     nil, #28
			     nil, #29
			     nil, #30
			     nil, #31
			     nil, #32
			     nil, #33
			     nil, #34
			     'Acoustic Bass Drum', #35
			     'Acoustic Bass Drum', #36
			     'Rim Shot', #37
			     'Acoustic Snare Drum', #38
			     'Hand Clap', #39
			     'Electronic Snare Drum', #40
			     'Acoustic Low Tom', #41
			     'Closed High Hat', #42
			     'Acoustic Low Tom', #43
			     'Open High Hat 1', #44
			     'Acoustic Middle Tom', #45
			     'Open High Hat 1', #46
			     'Acoustic Middle Tom', #47
			     'Acoustic High Tom', #48
			     'Crash Cymbal', #49
			     'Acoustic High Tom', #50
			     'Ride Cymbal', #51
			     nil, #52
			     nil, #53
			     'Tambourine', #54
			     nil, #55
			     'Cowbell', #56
			     nil, #57
			     nil, #58
			     nil, #59
			     'High Bongo', #60
			     'Low Bongo', #61
			     'Mute High Conga', #62
			     'High Conga', #63
			     'Low Conga', #64
			     'High Timbale', #65
			     'Low Timbale', #66
			     'High Agogo', #67
			     'Low Agogo', #68
			     'Cabasa', #69
			     'Maracas', #70
			     'Short Whistle', #71
			     'Long Whistle', #72
			     'Quijada', #73
			     nil, #74
			     'Claves', #75
			     'Laughing', #76
			     'Screaming', #77
			     'Punch', #78
			     'Heartbeat', #79
			     'Footsteps 1', #80
			     'Footsteps 2', #81
			     'Applause', #82
			     'Creaking', #83
			     'Door', #84
			     'Scratch', #85
			     'Windchime', #86
			     'Engine', #87
			     'Car-Stop', #88
			     'Car-Pass', #89
			     'Crash', #90
			     'Siren', #91
			     'Train', #92
			     'Jet', #93
			     'Helicopter', #94
			     'Starship', #95
			     'Pistol', #96
			     'Machine Gun', #97
			     'Lasergun', #98
			     'Explosion', #99
			     'Dog', #100
			     'Horse', #101
			     'Birds', #102
			     'Rain', #103
			     'Thunder', #104
			     'Wind', #105
			     'Waves', #106
			     'Stream', #107
			     'Bubble', #108
			     nil, #109
			     nil, #110
			     nil, #111
			     nil, #112
			     nil, #113
			     nil, #114
			     nil, #115
			     nil, #116
			     nil, #117
			     nil, #118
			     nil, #119
			     nil, #120
			     nil, #121
			     nil, #122
			     nil, #123
			     nil, #124
			     nil, #125
			     nil, #126
			     nil, #127
			   ],
	       },
	      ]

    DEVICE = '/dev/ttyS0'
    CHAN = 10

    NOTE_OFF = 0x80 + CHAN - 1
    NOTE_ON = 0x90 + CHAN - 1
    CTRL_CHG = 0xb0 + CHAN - 1
    PROG_CHG = 0xc0 + CHAN - 1

    def initialize()
	@prog_to_index = {}
	DRUMSET.each_with_index do |ds, i|
	    @prog_to_index[ds[:prog]] = i
	end
    end

    def open(dev = DEVICE)
	@fd = File.open(dev, "wb")
	@fd.write [0xf0, 0x41, 0x10, 0x42, 0x12,
		   0x40, 0x00, 0x7f, 0x00, 0x41, 0xf7].pack('C*') # GS Reset
    end

    def note_on(n, v = 127)
	@fd.write [NOTE_ON, n, v].pack('C*')
    end

    def note_off(n, v = 0)
	@fd.write [NOTE_OFF, n, v].pack('C*')
    end

    def prog_chg(prog)
	@fd.write [PROG_CHG, prog - 1].pack('C*')
    end

    def play_phrase(n)
	t = 0.3
	note_on(n); sleep(t); note_off(n)
	note_on(n); sleep(t); note_off(n)
	note_on(n); sleep(t); note_off(n)
	note_on(n); sleep(t); note_off(n)
	sleep(t)
    end

    def loop
	open()
	@curr_ds = DRUMSET[0]
	prog_chg(@curr_ds[:prog])

	while true
	    STDOUT.write "> "
	    s = gets
	    break if s == nil

	    case s
	    when /list_prog/
		DRUMSET.each do |ds|
		    puts "#{ds[:prog]} '#{ds[:name]}'"
		end
	    when /list_note/
		ds = @curr_ds
		ds[:list].each_with_index do |name, note|
		    next if name == nil
		    puts "prog #{ds[:prog]} (#{ds[:name]}): note #{note}: #{name}"
		end

	    when /\/(.*)\//
		regex = Regexp.new($1, Regexp::IGNORECASE)
		@done = {}
		DRUMSET.each do |ds|
		    prog_chg(ds[:prog])
		    (0..127).each do |n|
			nt_nm = ds[:list][n]
			next if nt_nm !~ regex
			if not @done[nt_nm]
			    puts "prog #{ds[:prog]} (#{ds[:name]}): note #{n}: #{nt_nm}"
			    play_phrase(n)
			    @done[nt_nm] = true
			end
		    end
		end

	    when /prog (\d+)/
		prog = $1.to_i
		i = @prog_to_index[prog]
		next if i == nil

		@curr_ds = DRUMSET[i]
		puts "prog #{prog} '#{@curr_ds[:name]}'"
		prog_chg(prog)

	    when /(\d+)-(\d+)/
		ds = @curr_ds
		($1.to_i .. $2.to_i).each do |n|
		    next if ds[:list][n] == nil
		    puts "prog #{ds[:prog]} (#{ds[:name]}): note #{n}: #{ds[:list][n]}"
		    play_phrase(n)
		end

	    when /(\d+)/
		n = $1.to_i
		ds = @curr_ds
		puts "prog #{ds[:prog]} (#{ds[:name]}): note #{n}: #{ds[:list][n]}"
		play_phrase(n)
	    end
	end
    end
end

tester = DrumTester.new()
tester.loop
