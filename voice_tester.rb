#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
#
# SC-88Pro ボイス確認
# 参考: http://www.eiji-s.info/index.html
#
# 全バリエーショントーン
# all:all
# nnn:all
# nnn-nnn:all
#
# 特定バリエーショントーン
# nnn:vvv
# nnn (キャピタルトーン。nnn:0と同じ)
# nnn-nnn (キャピタルトーンのみ)

class VoiceTester
    DEVICE = '/dev/ttyS0'
    CHAN = 1

    NOTE_OFF = 0x80 + CHAN - 1
    NOTE_ON = 0x90 + CHAN - 1
    CTRL_CHG = 0xb0 + CHAN - 1
    PROG_CHG = 0xc0 + CHAN - 1

    CC_BANK_SELM = 0
    CC_BANK_SELL = 32

    # PC => [[CC00, "SC88Pro Map"]...]
    VOICES = {
	1 => {
	    0 => "Piano 1",
	    8 => "Piano 1w",
	    16 => "European Pf",
	    24 => "Piano + Str.",
	},
	2 => {
	    0 => "Piano 2",
	    8 => "Piano 2w",
	    16 => "Dance Piano",
	},
	3 => {
	    0 => "Piano 3",
	    1 => "EG+Rhodes 1",
	    2 => "EG+Rhodes 2",
	    8 => "Piano 3w",
	},
	4 => {
	    0 => "Honky-tonk",
	    8 => "Honky-tonk 2",
	},
	5 => {
	    0 => "E.Piano 1",
	    8 => "St.Soft EP",
	    9 => "Cho. E.Piano",
	    10 => "SilentRhodes",
	    16 => "FM+SA EP",
	    17 => "Dist E.Piano",
	    24 => "Wurly",
	    25 => "Hard Rhodes",
	    26 => "MellowRhodes",
	},
	6 => {
	    0 => "E.Piano 2",
	    8 => "Detuned EP 2",
	    16 => "St.FM EP",
	    24 => "Hard FM EP",
	},
	7 => {
	    0 => "Harpsichord",
	    1 => "Harpsichord2",
	    8 => "Coupled Hps.",
	    16 => "Harpsi.w",
	    24 => "Harpsi.o",
	    32 => "Synth Harpsi",
	},
	8 => {
	    0 => "Clav.",
	    8 => "Comp Clav.",
	    16 => "Reso Clav.",
	    24 => "Clav.o",
	    32 => "Analog Clav.",
	    33 => "JP8 Clav. 1",
	    35 => "JP8 Clav. 2",
	},
	9 => {
	    0 => "Celesta",
	    1 => "Pop Celesta",
	},
	10 => {
	    0 => "Glockenspiel",
	},
	11 => {
	    0 => "Music Box",
	},
	12 => {
	    0 => "Vibraphone",
	    1 => "Pop Vibe.",
	    8 => "Vibraphone w",
	    9 => "Vibraphones",
	},
	13 => {
	    0 => "Marimba",
	    8 => "Marimba w",
	    16 => "Barafon",
	    17 => "Barafon 2",
	    24 => "Log drum",
	},
	14 => {
	    0 => "Xylophone",
	},
	15 => {
	    0 => "Tubular-bell",
	    8 => "Church Bell",
	    9 => "Carillon",
	},
	16 => {
	    0 => "Santur",
	    1 => "Santur 2",
	    8 => "Cimbalom",
	    16 => "Zither 1",
	    17 => "Zither 2",
	    24 => "Dulcimer",
	},
	17 => {
	    0 => "Organ 1",
	    1 => "Organ 101",
	    8 => "Trem. Organ",
	    9 => "Organ. o",
	    16 => "60's Organ 1",
	    17 => "60's Organ 2",
	    18 => "60's Organ 3",
	    19 => "Farf Organ",
	    24 => "Cheese Organ",
	    25 => "D-50 Organ",
	    26 => "JUNO Organ",
	    27 => "Hybrid Organ",
	    28 => "VS Organ",
	    29 => "Digi Church",
	    32 => "70's E.Organ",
	    33 => "Even Bar",
	    40 => "Organ Bass",
	    48 => "5th Organ",
	},
	18 => {
	    0 => "Organ 2",
	    1 => "Jazz Organ",
	    2 => "E.Organ 16+2",
	    8 => "Chorus Or.2",
	    9 => "Octave Organ",
	    32 => "Perc. Organ",
	},
	19 => {
	    0 => "Organ 3",
	    8 => "Rotary Org.",
	    16 => "Rotary Org.S",
	    17 => "Rock Organ 1",
	    18 => "Rock Organ 2",
	    24 => "Rotary Org.F",
	},
	20 => {
	    0 => "Church Org.1",
	    8 => "Church Org.2",
	    16 => "Church Org.3",
	    24 => "Organ Flute",
	    32 => "Trem.Flute",
	    33 => "Theater Org.",
	},
	21 => {
	    0 => "Reed Organ",
	    8 => "Wind Organ",
	},
	22 => {
	    0 => "Accordion Fr",
	    8 => "Accordion It",
	    9 => "Dist. Accord",
	    16 => "Cho. Accord",
	    24 => "Hard Accord",
	    25 => "Soft Accord",
	},
	23 => {
	    0 => "Harmonica",
	    1 => "Harmonica 2",
	},
	24 => {
	    0 => "Bandoneon",
	    8 => "Bandoneon 2",
	    16 => "Bandoneon 3",
	},
	25 => {
	    0 => "Nylon-str.Gt",
	    8 => "Ukulele",
	    16 => "Nylon Gt.o",
	    24 => "Velo Harmnix",
	    32 => "Nylon Gt 2",
	    40 => "Lequint Gt.",
	},
	26 => {
	    0 => "Steel-str.Gt",
	    8 => "12-str.Gt",
	    9 => "Nylon+Steel",
	    16 => "Mandolin",
	    17 => "Mandolin 2",
	    18 => "MandolinTrem",
	    32 => "Steel Gt.2",
	},
	27 => {
	    0 => "Jazz Gt.",
	    1 => "Mellow Gt.",
	    8 => "Pedal Steel",
	},
	28 => {
	    0 => "Clean Gt.",
	    1 => "Clean Half",
	    2 => "Open Hard 1",
	    3 => "Open Hard 2",
	    4 => "JC Clean Gt.",
	    8 => "Chorus Gt.",
	    9 => "JC Chorus Gt",
	    16 => "TC FrontPick",
	    17 => "TC Rear Pick",
	    18 => "TC Clean ff",
	    19 => "TC Clean 2:",
	},
	29 => {
	    0 => "Muted Gt.",
	    1 => "Muted Dis.Gt",
	    2 => "TC Muted Gt.",
	    8 => "Funk Pop",
	    16 => "Funk Gt.2",
	},
	30 => {
	    0 => "OverdriveGt",
	    1 => "Overdrive 2",
	    2 => "Overdrive 3",
	    3 => "More Drive",
	    8 => "LP OverDrvGt",
	    9 => "LP OverDrv:",
	},
	31 => {
	    0 => "DistortionGt",
	    1 => "Dist. Gt2:",
	    2 => "Dazed Guitar",
	    3 => "Distortion:",
	    4 => "Dist.Fast:",
	    8 => "Feedback Gt.",
	    9 => "Feedback Gt2",
	    16 => "Power Guitar",
	    17 => "Power Gt.2",
	    18 => "5th Dist.",
	    24 => "Rock Rhythm",
	    25 => "Rock Rhythm2",
	},
	32 => {
	    0 => "Gt.Harmonics",
	    8 => "Gt. Feedback",
	    9 => "Gt. Feedback2",
	    16 => "Ac.Gt.Harmnx",
	    24 => "E.Bass Harm.",
	},
	33 => {
	    0 => "Acoustic Bs.",
	    1 => "Rockabilly",
	    8 => "Wild A.Bass",
	    16 => "Bass + OHH",
	},
	34 => {
	    0 => "Fingered Bs.",
	    1 => "Fingered Bs2",
	    2 => "Jazz Bass",
	    3 => "Jazz Bass 2",
	    4 => "Rock Bass",
	    8 => "ChorusJazzBs",
	    16 => "F.Bass/Harm.",
	},
	35 => {
	    0 => "Picked Bass",
	    1 => "Picked Bass2",
	    2 => "Picked Bass3",
	    3 => "Picked Bass4",
	    8 => "Muted PickBs",
	    16 => "P.Bass/Harm.",
	},
	36 => {
	    0 => "Fretless Bs.",
	    1 => "Fretless Bs2",
	    2 => "Fretless Bs3",
	    3 => "Fretless Bs4",
	    4 => "Syn Fretless",
	    5 => "Mr.Smooth",
	    8 => "Wood+FlessBs",
	},
	37 => {
	    0 => "Slap Bass 1",
	    1 => "Slap Pop",
	    8 => "Reso Slap",
	    9 => "Unison Slap",
	},
	38 => {
	    0 => "Slap Bass 2",
	    8 => "FM Slap",
	},
	39 => {
	    0 => "Synth Bass 1",
	    1 => "SynthBass101",
	    2 => "CS Bass",
	    3 => "JP-4 Bass",
	    4 => "JP-8 Bass",
	    5 => "P5 Bass",
	    6 => "JPMG Bass",
	    8 => "Acid Bass",
	    9 => "TB303 Bass",
	    10 => "Tekno Bass",
	    11 => "TB303 Bass 2",
	    12 => "Kicked TB303",
	    13 => "TB303 Saw Bs",
	    14 => "Rubber303 Bs",
	    15 => "Reso 303 Bs",
	    16 => "Reso SH Bass",
	    17 => "303 Sqr Bs",
	    18 => "TB303 DistBs",
	    24 => "Arpeggio Bs",
	},
	40 => {
	    0 => "Synth Bass 2",
	    1 => "SynthBass201",
	    2 => "Modular Bass",
	    3 => "Seq Bass",
	    4 => "MG Bass",
	    5 => "Mg Oct Bass1",
	    6 => "MG Oct Bass2",
	    7 => "MG Blip Bs:",
	    8 => "Beef FM Bass",
	    9 => "Dly Bass",
	    10 => "X Wire Bass",
	    11 => "WireStr Bass",
	    12 => "Blip Bass:",
	    13 => "RubberBass 1",
	    16 => "RubberBass 2",
	    17 => "SH101 Bass 1",
	    18 => "SH101 Bass 2",
	    19 => "Smooth Bass",
	    20 => "SH101 Bass 3",
	    21 => "Spike Bass",
	    22 => "House Bass:",
	    23 => "KG Bass",
	    24 => "Sync Bass",
	    25 => "MG 5th Bass",
	    26 => "RND Bass",
	    27 => "WowMG Bass",
	    28 => "Bubble Bass",
	},
	41 => {
	    0 => "Violin :",
	    1 => "Violin Atk:",
	    8 => "Slow Violin",
	},
	42 => {
	    0 => "Viola :",
	    1 => "Viola Atk.:",
	},
	43 => {
	    0 => "Cello :",
	    1 => "Cello Atk.:",
	},
	44 => {
	    0 => "Contrabass",
	},
	45 => {
	    0 => "Tremolo Str",
	    8 => "Slow Tremolo",
	    9 => "Suspense Str",
	},
	46 => {
	    0 => "PizzicatoStr",
	    1 => "Vcs&Cbs Pizz",
	    2 => "Chamber Pizz",
	    3 => "St. Pizzicato",
	    8 => "Solo Pizz.",
	    16 => "Solo Spic.",
	},
	47 => {
	    0 => "Harp",
	    16 => "Synth Harp",
	},
	48 => {
	    0 => "Timpani",
	},
	49 => {
	    0 => "Strings :",
	    1 => "Bright Str:",
	    2 => "ChamberStr:",
	    3 => "Cello sect.",
	    8 => "Orchestra",
	    9 => "Orchestra 2",
	    10 => "Tremolo Orch",
	    11 => "Choir Str.",
	    12 => "Strings+Horn",
	    16 => "St. Strings",
	    24 => "Velo Strings",
	    32 => "Oct Strings1",
	    33 => "Oct Strings2",
	},
	50 => {
	    0 => "SlowStrings",
	    1 => "SlowStrings2",
	    8 => "Legato Str.",
	    9 => "Warm Strings",
	    10 => "St.Slow Str.",
	},
	51 => {
	    0 => "Syn.Strings1",
	    1 => "OB Strings",
	    2 => "StackStrings",
	    3 => "JP Strings",
	    8 => "Syn.Strings3",
	    9 => "Syn.Strings4",
	    16 => "High Strings",
	    17 => "Hybrid Str.",
	    24 => "Tron Strings",
	    25 => "Noiz Strings",
	},
	52 => {
	    0 => "Syn.Strings2",
	    1 => "Syn.Strings5",
	    2 => "JUNO Strings",
	    8 => "Air Strings",
	},
	53 => {
	    0 => "Choir Aahs",
	    8 => "St.ChoirAahs",
	    9 => "Melted Choir",
	    10 => "Church Choir",
	    16 => "Choir Hahs",
	    24 => "Chorus Lahs",
	    32 => "Chorus Aahs",
	    33 => "Male Aah+Str",
	},
	54 => {
	    0 => "Voice Oohs",
	    8 => "Voice Dahs",
	},
	55 => {
	    0 => "SynVox",
	    8 => "Syn.Voice",
	    9 => "Silent Night",
	    16 => "VP330 Choir",
	    17 => "Vinyl Choir",
	},
	56 => {
	    0 => "OrchestraHit",
	    8 => "Impact Hit",
	    9 => "Philly Hit",
	    10 => "Double Hit",
	    11 => "Perc. Hit",
	    12 => "Shock Wave",
	    16 => "Lo Fi Rave",
	    17 => "Techno Hit",
	    18 => "Dist. Hit",
	    19 => "Bam Hit",
	    20 => "Bit Hit",
	    21 => "Bim Hit",
	    22 => "Technorg Hit",
	    23 => "Rave Hit",
	    24 => "Strings Hit",
	    25 => "Stack Hit",
	},
	57 => {
	    0 => "Trumpet",
	    1 => "Trumpet 2",
	    2 => "Trumpet :",
	    8 => "Flugel Horn",
	    16 => "4th Trumpets",
	    24 => "Bright Tp.",
	    25 => "Warm Tp.",
	    32 => "Syn. Trumpet",
	},
	58 => {
	    0 => "Trombone",
	    1 => "Trombone 2",
	    2 => "Twin bones",
	    8 => "Bs. Trombone",
	},
	59 => {
	    0 => "Tuba",
	    1 => "Tuba 2",
	},
	60 => {
	    0 => "MutedTrumpet",
	    8 => "Muted Horns",
	},
	61 => {
	    0 => "French Horns",
	    1 => "Fr.Horn 2",
	    2 => "Horn + Orche",
	    3 => "Wide FreHrns",
	    8 => "F.Hrn Slow:",
	    9 => "Dual Horns",
	    16 => "Synth Horn",
	    24 => "F.Horn Rip",
	},
	62 => {
	    0 => "Brass 1",
	    1 => "Brass ff",
	    2 => "Bones Sect.",
	    8 => "Brass 2",
	    9 => "Brass 3",
	    10 => "Brass sfz",
	    16 => "Brass Fall",
	    17 => "Trumpet Fall",
	    24 => "Octave Brass",
	    25 => "Brass + Reed",
	},
	63 => {
	    0 => "SynthBrass1",
	    1 => "JUNO Brass",
	    2 => "StackBrass",
	    3 => "SH-5 Brass",
	    4 => "MKS Brass",
	    8 => "Pro Brass",
	    9 => "P5 Brass",
	    16 => "Oct SynBrass",
	    17 => "Hybrid Brass",
	},
	64 => {
	    0 => "Synth Brass 2",
	    1 => "Soft Brass",
	    2 => "Warm Brass",
	    8 => "SynBrass sfz",
	    9 => "OB Brass",
	    10 => "Reso Brass",
	    16 => "Velo Brass 1",
	    17 => "Transbrass",
	},
	65 => {
	    0 => "Soprano Sax",
	    8 => "Soprano Exp.",
	},
	66 => {
	    0 => "Alto Sax",
	    8 => "AltoSax Exp.",
	    9 => "Grow Sax",
	    16 => "AltoSax + Tp",
	},
	67 => {
	    0 => "Tenor Sax",
	    1 => "Tenor Sax :",
	    8 => "BreathyTn.:",
	    9 => "St. Tenor Sax",
	},
	68 => {
	    0 => "Baritone Sax",
	    1 => "Bari. Sax :",
	},
	69 => {
	    0 => "Oboe",
	    8 => "Oboe Exp.",
	    16 => "Multi Reed",
	},
	70 => {
	    0 => "English Horn",
	},
	71 => {
	    0 => "Bassoon",
	},
	72 => {
	    0 => "Clarinet",
	    8 => "Bs Clarinet",
	    16 => "Multi Wind",
	},
	73 => {
	    0 => "Piccolo",
	    1 => "Piccolo :",
	    8 => "Nay",
	    9 => "Nay Tremolo",
	    16 => "Di",
	},
	74 => {
	    0 => "Flute",
	    1 => "Flute 2 :",
	    2 => "Flute Exp.",
	    3 => "Flt Travelso",
	    8 => "Flute + Vln",
	    16 => "Tron Flute",
	},
	75 => {
	    0 => "Recorder",
	},
	76 => {
	    0 => "Pan Flute",
	    8 => "Kawala",
	    16 => "Zampona",
	    17 => "Zampona Atk",
	},
	77 => {
	    0 => "Bottle Blow",
	},
	78 => {
	    0 => "Shakuhachi",
	    1 => "Shakuhachi:",
	},
	79 => {
	    0 => "Whistle",
	    1 => "Whistle 2",
	},
	80 => {
	    0 => "Ocarina",
	},
	81 => {
	    0 => "Square Wave",
	    1 => "MG Square",
	    2 => "Hollow Mini",
	    3 => "Mellow FM",
	    4 => "CC Solo",
	    5 => "Shmoog",
	    6 => "LM Square",
	    8 => "2600 Sine",
	    9 => "Sine Lead",
	    10 => "KG Lead",
	    16 => "P5 Square",
	    17 => "OB Square",
	    18 => "JP-8 Square",
	    24 => "Pulse Lead",
	    25 => "JP8 PulseLd1",
	    26 => "JP8 PulseLd2",
	    27 => "MG Reso. Pls",
	},
	82 => {
	    0 => "Saw Wave",
	    1 => "OB2 Saw",
	    2 => "Pulse Saw",
	    3 => "Feline GR",
	    4 => "Big Lead",
	    5 => "Velo Lead",
	    6 => "GR-300",
	    7 => "LA Saw",
	    8 => "Doctor Solo",
	    9 => "Fat Saw Lead",
	    11 => "D-50 Fat Saw",
	    16 => "Waspy Synth",
	    17 => "PM Lead",
	    18 => "CS Saw Lead",
	    24 => "MG Saw 1",
	    25 => "MG Saw 2",
	    26 => "OB Saw 1",
	    27 => "OB Saw 2",
	    28 => "D-50 Saw",
	    29 => "SH-101 Saw",
	    30 => "CS Saw",
	    31 => "MG Saw Lead",
	    32 => "OB Saw Lead",
	    33 => "P5 Saw Lead",
	    34 => "MG unison",
	    35 => "Oct Saw Lead",
	    40 => "SequenceSaw1",
	    41 => "SequenceSaw2",
	    42 => "Reso Saw",
	    43 => "Cheese Saw 1",
	    44 => "Cheese Saw 2",
	    45 => "Rhythmic Saw",
	},
	83 => {
	    0 => "Syn.Calliope",
	    1 => "Vent Synth",
	    2 => "Pure PanLead",
	},
	84 => {
	    0 => "Chiffer Lead",
	    1 => "TB Lead",
	    8 => "Mad Lead",
	},
	85 => {
	    0 => "Charang",
	    8 => "Dist.Lead",
	    9 => "Acid Guitar1",
	    10 => "Acid Guitar2",
	    16 => "P5 Sync Lead",
	    17 => "Fat Sync Lead",
	    18 => "Rock Lead",
	    19 => "5th DecaSync",
	    20 => "Dirty Sync",
	    24 => "JUNO Sub Osc",
	},
	86 => {
	    0 => "Solo Vox",
	    8 => "Vox Lead",
	    9 => "LFO Vox",
	},
	87 => {
	    0 => "5th Saw Wave",
	    1 => "Big Fives",
	    2 => "5th Lead",
	    3 => "5th Ana.Clav",
	    8 => "4th Lead",
	},
	88 => {
	    0 => "Bass & Lead",
	    1 => "Big & Raw",
	    2 => "Fat & Perky",
	    3 => "JUNO Rave",
	    4 => "JP8 BsLead 1",
	    5 => "JP8 BsLead 2",
	    6 => "SH-5 Bs.Lead",
	},
	89 => {
	    0 => "Fantasia",
	    1 => "Fantasia 2",
	    2 => "New Age Pad",
	    3 => "Bell Heaven",
	},
	90 => {
	    0 => "Warm Pad",
	    1 => "Thick Matrix",
	    2 => "Horn Pad",
	    3 => "Rotary Strng",
	    4 => "OB Soft Pad",
	    8 => "Octave Pad",
	    9 => "Stack Pad",
	},
	91 => {
	    0 => "Polysynth",
	    1 => "80's PolySyn",
	    2 => "Polysynth 2",
	    3 => "Poly King",
	    8 => "Power Stack",
	    9 => "Octave Stack",
	    10 => "Reso Stack",
	    11 => "Techno Stack",
	},
	92 => {
	    0 => "Space Voice",
	    1 => "Heaven II",
	    2 => "SC Heaven",
	    8 => "Cosmic Voice",
	    9 => "Auh Vox",
	    10 => "AuhAuh",
	    11 => "Vocorderman",
	},
	93 => {
	    0 => "Bowed Glass",
	    1 => "SoftBellPad",
	    2 => "JP8 Sqr Pad",
	    3 => "7thBelPad",
	},
	94 => {
	    0 => "Metal Pad",
	    1 => "Tine Pad",
	    2 => "Panner Pad",
	},
	95 => {
	    0 => "Halo Pad",
	    1 => "Vox Pad",
	    2 => "Vox Sweep",
	    8 => "Horror Pad",
	},
	96 => {
	    0 => "Sweep Pad",
	    1 => "Polar Pad",
	    8 => "Converge",
	    9 => "Shwimmer",
	    10 => "Celestial Pd",
	    11 => "Bag Sweep",
	},
	97 => {
	    0 => "Ice Rain",
	    1 => "Harmo Rain",
	    2 => "African wood",
	    3 => "Anklung Pad",
	    4 => "Rattle Pad",
	    8 => "Clavi Pad",
	},
	98 => {
	    0 => "Soundtrack",
	    1 => "Ancestral",
	    2 => "Prologue",
	    3 => "Prologue 2",
	    4 => "Hols Strings",
	    8 => "Rave",
	},
	99 => {
	    0 => "Crystal",
	    1 => "Syn Mallet",
	    2 => "Soft Crystal",
	    3 => "Round Glock",
	    4 => "Loud Glock",
	    5 => "GlockenChime",
	    6 => "Clear Bells",
	    7 => "ChristmasBel",
	    8 => "Vibra Bells",
	    9 => "Digi Bells",
	    10 => "Music Bell",
	    11 => "Analog Bell",
	    16 => "Choral Bells",
	    17 => "Air Bells",
	    18 => "Bell Harp",
	    19 => "Gamelimba",
	    20 => "JUNO Bell",
	},
	100 => {
	    0 => "Atmosphere",
	    1 => "Warm Atmos",
	    2 => "Nylon Harp",
	    3 => "Harpvox",
	    4 => "HollowReleas",
	    5 => "Nylon+Rhodes",
	    6 => "Ambient Pad",
	    7 => "Invisible",
	    8 => "Pulsey Key",
	    9 => "Noise Piano",
	},
	101 => {
	    0 => "Brightness",
	    1 => "Shining Star",
	    2 => "OB Stab",
	    8 => "Org Bell",
	},
	102 => {
	    0 => "Goblin",
	    1 => "Goblinson",
	    2 => "50's Sci-Fi",
	    3 => "Abduction",
	    4 => "Auhbient",
	    5 => "LFO Pad",
	    6 => "Random Str",
	    7 => "Random Pad",
	    8 => "LowBirds Pad",
	    9 => "Falling Down",
	    10 => "LFO RAVE",
	    11 => "LFO Horror",
	    12 => "LFO Techno",
	    13 => "Alternative",
	    14 => "UFO FX",
	    15 => "Gargle Man",
	    16 => "Sweep FX",
	},
	103 => {
	    0 => "Echo Drops",
	    1 => "Echo Bell",
	    2 => "Echo Pan",
	    3 => "Echo Pan 2",
	    4 => "Big Panner",
	    5 => "Reso Panner",
	    6 => "Water Piano",
	    8 => "Pan Sequence",
	    9 => "Aqua",
	},
	104 => {
	    0 => "Star Theme",
	    1 => "Star Theme 2",
	    8 => "Dream Pad",
	    9 => "Silky Pad",
	    16 => "New Century",
	    17 => "7th Atmos.",
	    18 => "Galaxy Way",
	},
	105 => {
	    0 => "Sitar",
	    1 => "Sitar 2",
	    2 => "Detune Sitar",
	    3 => "Sitar 3",
	    8 => "Tambra",
	    16 => "Tamboura",
	},
	106 => {
	    0 => "Banjo",
	    1 => "Muted Banjo",
	    8 => "Rabab",
	    9 => "San Xian",
	    16 => "Gopichant",
	    24 => "Oud",
	    28 => "Oud+Strings",
	    32 => "Pi Pa",
	},
	107 => {
	    0 => "Shamisen",
	    1 => "Tsugaru",
	    8 => "Syn Shamisen",
	},
	108 => {
	    0 => "Koto",
	    1 => "Gu Zheng",
	    8 => "Taisho Koto",
	    16 => "Kanoon",
	    19 => "Kanoon+Choir",
	    24 => "Oct Harp",
	},
	109 => {
	    0 => "Kalimba",
	    8 => "Sanza",
	},
	110 => {
	    0 => "Bagpipe",
	    8 => "Didgeridoo",
	},
	111 => {
	    0 => "Fiddle",
	    8 => "Er Hu",
	    9 => "Gao Hu",
	},
	112 => {
	    0 => "Shanai",
	    1 => "Shanai 2",
	    8 => "Pungi",
	    16 => "Hichiriki",
	    24 => "Mizmar",
	    32 => "Suona 1",
	    33 => "Suona 2",
	},
	113 => {
	    0 => "Tinkle Bell",
	    8 => "Bonang",
	    9 => "Gender",
	    10 => "Gamelan Gong",
	    11 => "St.Gamelan",
	    12 => "Jang-Gu",
	    16 => "RAMA Cymbal",
	},
	114 => {
	    0 => "Agogo",
	    8 => "Atarigane",
	    16 => "Tambourine",
	},
	115 => {
	    0 => "Steel Drums",
	    1 => "Island Mlt",
	},
	116 => {
	    0 => "Woodblock",
	    8 => "Castanets",
	    16 => "Angklung",
	    17 => "Angkl Rhythm",
	    24 => "Finger Snaps",
	    32 => "909 HandClap",
	},
	117 => {
	    0 => "Taiko",
	    1 => "Small Taiko",
	    8 => "Concert BD",
	    16 => "Jungle BD",
	    17 => "Techno BD",
	    18 => "Bounce",
	},
	118 => {
	    0 => "Melo. Tom 1",
	    1 => "Real Tom",
	    8 => "Melo. Tom 2",
	    9 => "Rock Tom",
	    16 => "Rash SD",
	    17 => "House SD",
	    18 => "Jungle SD",
	    19 => "909 SD",
	},
	119 => {
	    0 => "Synth Drum",
	    8 => "808 Tom",
	    9 => "Elec Perc",
	    10 => "Sine Perc.",
	    11 => "606 Tom",
	    12 => "909 Tom",
	},
	120 => {
	    0 => "Reverse Cym.",
	    1 => "Reverse Cym2",
	    2 => "Reverse Cym3",
	    8 => "Rev.Snare 1",
	    9 => "Rev.Snare 2",
	    16 => "Rev.Kick 1",
	    17 => "Rev.ConBD",
	    24 => "Rev.Tom 1",
	    25 => "Rev.Tom 2",
	},
	121 => {
	    0 => "Gt.FretNoise",
	    1 => "Gt.Cut Noise",
	    2 => "String Slap",
	    3 => "Gt.CutNoise2",
	    4 => "Dist.CutNoiz",
	    5 => "Bass Slide",
	    6 => "Pick Scrape",
	    8 => "Gt. FX Menu",
	    9 => "Bartok Pizz.",
	    10 => "Guitar Slap",
	    11 => "Chord Stroke",
	    12 => "Biwa Stroke",
	    13 => "Biwa Tremolo",
	},
	122 => {
	    0 => "Breath Noise",
	    1 => "Fl.Key Click",
	},
	123 => {
	    0 => "Seashore",
	    1 => "Rain",
	    2 => "Thunder",
	    3 => "Wind",
	    4 => "Stream",
	    5 => "Bubble",
	    6 => "Wind 2",
	    16 => "Pink Noise",
	    17 => "White Noise",
	},
	124 => {
	    0 => "Bird",
	    1 => "Dog",
	    2 => "Horse-Gallop",
	    3 => "Bird 2",
	    4 => "Kitty",
	    5 => "Growl",
	},
	125 => {
	    0 => "Telephone 1",
	    1 => "Telephone 2",
	    2 => "DoorCreaking",
	    3 => "Door",
	    4 => "Scratch",
	    5 => "Wind Chimes",
	    7 => "Scratch 2",
	    8 => "ScratchKey",
	    9 => "TapeRewind",
	    10 => "Phono Noise",
	    11 => "MC-500 Beep",
	},
	126 => {
	    0 => "Helicopter",
	    1 => "Car-Engine",
	    2 => "Car-Stop",
	    3 => "Car-Pass",
	    4 => "Car-Crash",
	    5 => "Siren",
	    6 => "Train",
	    7 => "Jetplane",
	    8 => "Starship",
	    9 => "Burst Noise",
	    10 => "Calculating",
	    11 => "Perc. Bang",
	},
	127 => {
	    0 => "Applause",
	    1 => "Laughing",
	    2 => "Screaming",
	    3 => "Punch",
	    4 => "Heart Beat",
	    5 => "Footsteps",
	    6 => "Applause 2",
	    7 => "Small Club",
	    8 => "ApplauseWave",
	    16 => "Voice One",
	    17 => "Voice Two",
	    18 => "Voice Three",
	    19 => "Voice Tah",
	    20 => "Voice Whey",
	},
	128 => {
	    0 => "Gun Shot",
	    1 => "Machine Gun",
	    2 => "Lasergun",
	    3 => "Explosion",
	    4 => "Eruption",
	    5 => "Big Shot",
	},
    }

    def open(dev = DEVICE)
	@fd = File.open(dev, "wb")
	@fd.write [0xf0, 0x41, 0x10, 0x42, 0x12,
		   0x40, 0x00, 0x7f, 0x00, 0x41, 0xf7].pack('C*') # GS Reset
	@base_note = 60
    end

    def note_on(n, v = 127)
	@fd.write [NOTE_ON, n, v].pack('C*')
    end

    def note_off(n, v = 0)
	@fd.write [NOTE_OFF, n, v].pack('C*')
    end

    def prog_chg(prog)
	@fd.write [PROG_CHG, prog - 1].pack('C*')
    end

    def cc0_chg(bank)
	@fd.write [CTRL_CHG, CC_BANK_SELM, bank].pack('C*')
    end

    def play_phrase
	n = @base_note
	t = 0.3
	note_on(n + 0); sleep(t); note_off(n + 0)
	note_on(n + 2); sleep(t); note_off(n + 2)
	note_on(n + 4); sleep(t); note_off(n + 4)
	note_on(n + 2); sleep(t); note_off(n + 2)
	note_on(n + 0); sleep(t); note_off(n + 0)
	sleep(t)
    end

    def test_regex_matched(regex)
	(1..128).each do |prog|
	    VOICES[prog].each do |bank, name|
		if regex =~ name
		    puts "prog #{prog} variation #{bank}: #{name}"
		    cc0_chg(bank)
		    prog_chg(prog)
		    play_phrase()
		end
	    end
	end
    end

    def test_all_variants(prog)
	VOICES[prog].each do |bank, name|
	    puts "prog #{prog} variation #{bank}: #{name}"
	    cc0_chg(bank)
	    prog_chg(prog)
	    play_phrase()
	end
    end

    def test_voice(prog, bank)
	return if VOICES[prog] == nil
	return if VOICES[prog][bank] == nil
	puts "prog #{prog} variation #{bank}: #{VOICES[prog][bank]}"
	cc0_chg(bank)
	prog_chg(prog)
	play_phrase()
    end

    def loop
	open()
	while true
	    STDOUT.write "> "
	    s = gets
	    break if s == nil

	    case s
	    when /base (\d+)/
		@base_note = $1.to_i

	    when /^\/(.+)\/$/
		regex = Regexp.new($1, Regexp::IGNORECASE)
		test_regex_matched(regex)

	    when /(\d+)-(\d+):all/
		($1.to_i .. $2.to_i).each do |prog|
		    test_all_variants(prog)
		end
	    when /(\d+):all/
		test_all_variants($1.to_i)
	    when /all:all/
		(1..128).each do |voice|
		    test_all_variants(voice)
		end

	    when /(\d+):(\d+)/
		test_voice($1.to_i, $2.to_i)
	    when /(\d+)-(\d+)/
		($1.to_i .. $2.to_i).each do |prog|
		    test_voice(prog, 0)
		end
	    when /(\d+)/
		test_voice($1.to_i, 0)
	    end
	end
    end
end

tester = VoiceTester.new()
tester.loop
